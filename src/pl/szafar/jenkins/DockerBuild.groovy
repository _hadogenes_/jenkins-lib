package pl.szafar.jenkins

class DockerBuild extends Notifications {
    def agentLabel = [ 'x86', 'dockerbuild' ]

    DockerBuild(def jenkins) {
        super(jenkins)
    }

    def init(def body) {
        def pipelineParams = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = pipelineParams
        body()

        if (pipelineParams.containsKey('addonalLabel')) {
            agentLabel += pipelineParams.addonalLabel
        }

        withFolderProperties {
            if (env.ADDONAL_DOCKER_LABEL) {
                agentLabel << env.ADDONAL_DOCKER_LABEL
            }
        }
    }

    def build_step() {
        withFolderProperties {
            git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_BASE_URL + currentBuild.projectName

            def dockerDefaultRegistry = "docker.io"
            def docekrImageName = env.DOCKER_NAMESPACE + "/" + currentBuild.projectName
            def dockerRegistryUrl = null

            if (dockerDefaultRegistry == env.DOCKER_REGISTRY) {
                dockerRegistryUrl = "https://index.docker.io/v1/"
            } else {
                docekrImageName = env.DOCKER_REGISTRY + "/" + docekrImageName
                dockerRegistryUrl = "https://${env.DOCKER_REGISTRY}/v2/"
            }
                
            docker.withRegistry(dockerRegistryUrl, env.DOCKER_CREDENTIALS_ID) {
                def dockerImage = docker.build(docekrImageName, '--no-cache .')
                dockerImage.push()
            }
        }
    }

    def cleanup_step() {
        deleteDir()
        sh '''
            docker ps -aq | xargs docker rm > /dev/null 2>&1 || true
            docker image prune --all --force > /dev/null 2>&1 || true
            docker volume prune --force > /dev/null 2>&1 || true
        '''
    }
}
