package pl.szafar.jenkins

abstract class CommonTools extends PassthroughEnv {
    ArrayList setNiceCmd = [ '#!/bin/bash -xe', '[ -z "$NICE_LEVEL" ] || renice -n $NICE_LEVEL $$' ]

    CommonTools(def jenkins) {
        super(jenkins)
    }

    String expandString(String str, def binding) {
        def engine = new groovy.text.SimpleTemplateEngine()
        return engine.createTemplate(str).make(binding).toString()
    }

    String basename(String path) {
        def pathTab = path.split('/')
        return pathTab[pathTab.length - 1]
    }

    String shCwd(String cwd, def cmd) {
        if (!cmd) {
            return ""
        }

        return joinSh([ "cd ${cwd}" ] + cmd, true)
    }

    String joinSh(ArrayList cmdTab, boolean oneLine = false) {
        if (oneLine) {
            return "( ${cmdTab.join(" && ")} )"
        }
        else {
            return cmdTab.join('\n')
        }
    }

    String shParallel(ArrayList cmdTab) {
        if (!cmdTab) {
            return ""
        }

        return """echo "${joinSh(cmdTab)}" | parallel"""
    }

    def runShList(ArrayList cmdTab) {
        if (cmdTab) {
            sh joinSh(setNiceCmd + cmdTab)
        }
    }
}
