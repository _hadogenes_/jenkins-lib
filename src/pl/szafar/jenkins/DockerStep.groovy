package pl.szafar.jenkins

abstract class DockerStep extends Notifications {
    def dockerImage = []
    def dockerArgs = null
    
    DockerStep(def jenkins) {
        super(jenkins)
    }

    def isAgentRunningDocker() {
        return ('docker' in env.NODE_LABELS.tokenize())
    }

    def runStep(boolean onlyFirstImage = false, def step) {
        if (isAgentRunningDocker()) {
            dockerImage.any {
                def image = docker.image(it)
                image.pull()
                if (dockerArgs != null) {
                    image.inside(dockerArgs) {
                        step()
                    }
                }
                else {
                    image.inside {
                        step()
                    }
                }

                if (onlyFirstImage) {
                    return true // break
                }
            }
        } else {
            step()
        }
    }
}
