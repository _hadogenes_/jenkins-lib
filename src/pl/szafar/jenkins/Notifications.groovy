package pl.szafar.jenkins

abstract class Notifications extends CommonTools {
    Notifications(def jenkins) {
        super(jenkins)
    }
    
    def notify(String title, String message, int priority = 1) {
        withFolderProperties {
            String requestData = """
            {
                "extras": {
                    "client::notification": {
                        "click": {
                            "url": "${env.BUILD_URL}console"
                        }
                    }
                },
                "title": "[Jenkins] ${title}",
                "message": "${message}",
                "priority": ${priority}
            }
            """
            withCredentials([string(credentialsId: env.GOTIFY_CREDENTIALS_ID, variable: 'GOTIFY_TOKEN')]) {
                httpRequest(
                    url: "${env.GOTIFY_URL}/message",
                    contentType: 'APPLICATION_JSON',
                    httpMode: 'POST',
                    responseHandle: 'NONE',
                    customHeaders: [[ name: 'X-Gotify-Key', value: env.GOTIFY_TOKEN ]],
                    requestBody: requestData
                )
            }
        }
    }

    def error(String msg) {
        notify("Error in ${currentBuild.projectName}", msg, 5)
        return super.error(msg)
    }

    def notifyFixed() {
        notify("Success ${currentBuild.projectName}", "${currentBuild.fullProjectName} is fixed", 3)
    }

    def notifyRegression() {
        notify("Failture ${currentBuild.projectName}", "${currentBuild.fullProjectName} is failed", 4)
    }
}
