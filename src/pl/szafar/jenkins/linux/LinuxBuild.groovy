package pl.szafar.jenkins.linux

import pl.szafar.jenkins.DockerStep

abstract class LinuxBuild extends DockerStep {
    def common
    def triggerCron = '0 12 * * 6'
    def triggerUrlCron = '0 */2 * * *'
    def urlTriggerEntries = []
    def buildAgentLabel = [ 'x86' ]
    def preBuildAgentLabel = [ 'x86', 'prebuild' ]
    def packagesToBuild = []
    def isStartedByUser

    def pipelineParams = [
        forceBuild: false,
        longbuild: false,
        repackbuild: false,
        quickbuild: false,
        add32Bit: false,
        withDeps: false,
        addonalPackages: [],
        addonalDepends: [],
        addonalDependsForce: [],
        manualDownload: false,
        addonalPreBuildLabel: [],
        addonalBuildLabel: [],
        ignoreJobName: false,
        ignorePackages: [],
    ]

    LinuxBuild(def jenkins) {
        super(jenkins)
    }
    
    def init(body) {
        def argPipelineParams = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = argPipelineParams
        body()
        
        pipelineParams << argPipelineParams

        if (pipelineParams.addonalPreBuildLabel instanceof String) {
            pipelineParams.addonalPreBuildLabel = [ pipelineParams.addonalPreBuildLabel ]
        }

        if (pipelineParams.addonalBuildLabel instanceof String) {
            pipelineParams.addonalBuildLabel = [ pipelineParams.addonalBuildLabel ]
        }

        if (pipelineParams.longbuild) {
            buildAgentLabel += [ 'longbuild' ]
        } else if (pipelineParams.quickbuild) {
            buildAgentLabel += [ 'quickbuild' ]
        } else if (pipelineParams.repackbuild) {
            buildAgentLabel += [ 'repackbuild' ]
        } else {
            buildAgentLabel += [ 'normalbuild' ]
        }

        isStartedByUser = (currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause) != null)
        preBuildAgentLabel += pipelineParams.addonalPreBuildLabel
        buildAgentLabel += pipelineParams.addonalBuildLabel

        packagesToBuild = pipelineParams.addonalPackages
        if (!pipelineParams.ignoreJobName) {
            packagesToBuild += currentBuild.projectName
        }

        withFolderProperties {
            dockerImage += env.DOCKER_IMAGE

            if (pipelineParams.add32Bit) {
                dockerImage += env.DOCKER_IMAGE_32BIT
            }
        }
        
        return argPipelineParams
    }
    
    def prepare(boolean preBuild = false) {
        echo("Build step: prepare")
        common.setupEnv()
        common.systemUpdate(isAgentRunningDocker(), !preBuild)
        common.getTools(preBuild)
        if (!preBuild) {
            common.importGpgKey()
            common.importGpgKeyToSystem()
        }
    }

    def download(boolean preBuild = false) {
        echo("Build step: download")
    }
    def preBuild() {
        echo("Build step: preBuild")
    }
    def extract() {
        echo("Build step: extract")
    }
    def building() {
        echo("Build step: building")
    }
    
    def prebuild_step() {
        runStep(true) {
            prepare(true)
            download(true)
            preBuild()
        }

        if (packagesToBuild.size() > 0) {
            echo "Packages to build: ${packagesToBuild.join(" ")}"
            notify("Building ${currentBuild.projectName}", "Packages to build: ${packagesToBuild.join(" ")}")
        }
        else {
            echo "Nothing to build"
        }
    }

    def build_step() {
        runStep {
            prepare()
            download()
            extract()
            building()
        }
    }

    def cleanup_step() {
        common.cleanup(isAgentRunningDocker())
    }
}
