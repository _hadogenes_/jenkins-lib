package pl.szafar.jenkins.linux

import pl.szafar.jenkins.CommonTools

abstract class LinuxCommon extends CommonTools {
    boolean is32Bit = false

    LinuxCommon(def jenkins) {
        super(jenkins)
    }

    def systemUpdate(boolean isAgentRunningDocker, boolean upgradeSystem = true) {}

    def setupEnv() {
        env.THREAD_NUM = sh(script:'nproc', returnStdout: true).trim()
        env.XZ_OPT = "-T${env.THREAD_NUM}"
        env.ZSTD_NBTHREADS = env.THREAD_NUM
    }

    def addCustomRepo() {}

    def getTools(boolean preBuild = false) {
        withFolderProperties {
            if (env.GIT_TOOLS_URL) {
                dir("tools") {
                    git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_TOOLS_URL
                    if (fileExists('scripts')) {
                        sh 'ln -srf $PWD/scripts $WORKSPACE'
                    }
                }
            }

            if (env.GIT_PACKAGES_PATCH_URL) {
                dir("patch") {
                    git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_PACKAGES_PATCH_URL
                }
            }

            if (!preBuild) {
                if (env.GIT_COMPILATION_FLAGS_URL) {
                    dir("compilation-flags") {
                        git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_COMPILATION_FLAGS_URL
                    }
                }

                if (env.GIT_SPECS_URL) {
                    dir("specs") {
                        git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_SPECS_URL
                    }
                }
            }
        }
    }

    def importGpgKey() {
        withFolderProperties {
            withCredentials([file(credentialsId: env.GPG_CREDENTIALS_ID, variable: 'GPG_PRIVATE_KEY')]) {
                sh '''
                    gpg --batch --import "$GPG_PRIVATE_KEY" > /dev/null 2>&1
                    echo -e "5\ny\n" |  gpg --batch --command-fd 0 --expert --edit-key $GPGKEY trust > /dev/null 2>&1

                    rm -f public-key.gpg
                    gpg --batch --armor --output public-key.gpg --export "$GPGKEY" > /dev/null 2>&1

                    (
                        echo use-agent
                        echo pinentry-mode loopback
                        echo cert-digest-algo SHA512
                        echo digest-algo SHA512
                        echo personal-digest-preferences SHA512
                        echo disable-cipher-algo SHA1
                        echo default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed

                        echo default-key $GPGKEY
                    ) > "$HOME/.gnupg/gpg.conf"

                    (
                        echo allow-loopback-pinentry
                    ) > "$HOME/.gnupg/gpg-agent.conf"
                '''
            }
        }
    }
    
    def importGpgKeyToSystem() {}
    
    def cleanup(boolean isAgentRunningDocker = false) {
        def dirToDelete = [
            pwd()
        ]

        dirToDelete.each {
            dir(it) {
                deleteDir()
            }
        }

        if (!isAgentRunningDocker) {
            withFolderProperties {
                sh '''
                    gpg --fingerprint --with-colons $GPGKEY |\
                        sed -n 's/^fpr:::::::::\\([[:alnum:]]\\+\\):/\\1/p' |\
                        xargs --no-run-if-empty \
                            gpg --batch --yes --delete-secret-keys || true
                '''
            }

            dir(env.WORKSPACE + "/..") {
                def dirToDeleteTmp = dirToDelete.collect { it + "@tmp" }
                sh "rm -rf ${ dirToDeleteTmp.join(' ') }"
            }
        }
    }
}
