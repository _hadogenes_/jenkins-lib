package pl.szafar.jenkins.linux.debian

class DebianPackage {
    static String versionsSlightlyHigher(String version) {
        def versionTab = version.split('-')
        if (versionTab.size() >= 2) {
            def versionMinorTab = versionTab[1].split('\\+')
            def binMt = versionTab[1] =~ /\+b([0-9]+)/
            if (binMt.find()) {
                def binRev = binMt[0][1].toInteger() + 1
                versionMinorTab = versionMinorTab.findAll { !(it ==~ /b[0-9]+/) }
                versionMinorTab[0] += "." + binRev.toString()
            } else {
                versionMinorTab[0] += ".1"
            }
            versionTab[1] = versionMinorTab.join('+')
        }
        else {
            versionTab += "1"
        }
        return versionTab.join('-')
    }

    static String compareVersion(String ver1, String ver2) {
        def ver1Tab = ver1.tokenize('.+-:~')
        def ver2Tab = ver2.tokenize('.+-:~')

        for (int i = 0; i < Integer.min(ver1Tab.size(), ver2Tab.size()); ++i) {
            if (ver1Tab[i] != ver2Tab[i]) {
                if (ver1Tab[i].isNumber() && ver2Tab[i].isNumber()) {
                    def ver1int = new Integer(ver1Tab[i])
                    def ver2int = new Integer(ver2Tab[i])

                    if (ver1int > ver2int) {
                        return ver1
                    }
                    else {
                        return ver2
                    }
                }
                else if (ver1Tab[i].isNumber()) {
                    return ver1
                }
                else if (ver2Tab[i].isNumber()) {
                    return ver2
                }
                else {
                    if (ver1Tab[i] > ver2Tab[i]) {
                        return ver1
                    }
                    else {
                        return ver2
                    }
                }
            }
        }

        if (ver1Tab.size() > ver2Tab.size()) {
            return ver1
        }
        else {
            return ver2
        }
    }

    String name = null
    String version = null
    String sourceVersion = null
    String fullVersion = null
    String gitName = null
    String gitTag = "master"
    DebianPackage base = null
    String dsc = null
    String deb = null
    Boolean virtual = false
    def preBuildCmd = []
    def postBuildCmd = []

    DebianPackage(String name, String deb = null, String fullVersion = null, String sourceVersion = null, String version = null) {
        this.name = name
        this.gitName = name
        this.version = version
        this.fullVersion = fullVersion
        this.sourceVersion = sourceVersion
        this.deb = deb

        this.base = this
    }

    void setVersionAndDeb(String ver) {
        if (ver != this.version) {
            this.version = ver
            def debArch = ""

            if (this.deb != null) {
                def destpkgTab = this.deb.split('_')
                debArch = destpkgTab[destpkgTab.length - 1]
            }

            this.deb = "${this.name}_${this.version}_${debArch}"
        }
    }
}
