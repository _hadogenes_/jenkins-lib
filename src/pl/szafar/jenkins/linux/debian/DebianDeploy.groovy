package pl.szafar.jenkins.linux.debian

import pl.szafar.jenkins.linux.LinuxDeploy

class DebianDeploy extends LinuxDeploy {
    DebianDeploy(jenkins) {
        super(jenkins)
        common = new DebianCommon(jenkins)
    }

    def init(body) {
        super.init(body)

        repoPath = "/repo/${params.debianRelease}"
        agentLabel << 'debianrepo'
    }

    def deploy() {
        def deployCmd = []
        def repoPkgList = [:]

        findFiles(glob: '*.deb').each {
            def repoName = params.repoName
            if (it.name.contains('-dbgsym') || it.name.contains('-dbg')) {
                repoName = "debug"
            }
            
            if (!repoPkgList.containsKey(repoName)) {
                repoPkgList[repoName] = []
            }
            
            repoPkgList[repoName] += it.path
        }

        def repreproLockfile = "${repoPath}/db/lockfile"
        if (fileExists(repreproLockfile)) {
            deployCmd += [
                'sleep 1m',
                "rm -f ${repreproLockfile}"
            ]
        }

        //if (params.forceAdd) {
            repoPkgList.each {
                def pkgList = []
                it.value.each { pkgFilePath ->
                    def pkgFileName = new File(pkgFilePath).getName()
                    def pkgFileNameTab = pkgFileName.split('_')
                    pkgList += pkgFileNameTab[0]
                }

                deployCmd += "reprepro --verbose --basedir '${repoPath}' --component ${it.key} remove ${params.debianRelease} ${pkgList.join(' ')}"
            }
        //}

        repoPkgList.each { repoName, pkgList ->
            def pkgListNoPriority = []

            pkgList.clone().each {
                def result = sh(
                    script: """
                        dpkg -e ${it}
                        grep -q Priority DEBIAN/control
                    """,
                    returnStatus: true
                )

                if (result != 0) {
                    pkgListNoPriority += it
                    pkgList -= it
                }
            }

            if (pkgList.size() > 0) {
                deployCmd += """
                    reprepro --verbose \
                        --basedir '${repoPath}' \
                        --component ${repoName} \
                        includedeb ${params.debianRelease} \
                        ${pkgList.join(' ')}
                """
            }

            if (pkgListNoPriority.size() > 0) {
                deployCmd += """
                    reprepro --verbose \
                        --basedir '${repoPath}' \
                        --component ${repoName} \
                        --priority optional \
                        includedeb ${params.debianRelease} \
                        ${pkgListNoPriority.join(' ')}
                """
            }

        }

        sh joinSh(deployCmd)
    }

    def listAllPackagesToFile() {
        sh """
            find '${repoPath}/dists/${params.debianRelease}/${params.repoName}' -name Packages -exec grep '^Filename:' {} + |\
                xargs -n 1 -d '\\n'  basename |\
                sort -u > '${params.debianRelease}-${params.repoName}.list'
        """
    }
}
