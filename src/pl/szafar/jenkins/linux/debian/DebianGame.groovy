package pl.szafar.jenkins.linux.debian

import pl.szafar.jenkins.linux.LinuxGame

class DebianGame extends LinuxGame {

    DebianGame(jenkins) {
        super(jenkins)
        common = new DebianCommon(jenkins)
        playitSystemPath = "/usr/share/games/play.it"
    }

    def init(body) {
        super.init(body)

        preBuildAgentLabel << "(debian||docker)"
        buildAgentLabel << "(debian||docker)"
    }

    def prepare(boolean preGame = false) {
        super.prepare(preGame)

        common.debianRelease = sh(script: 'lsb_release -cs 2> /dev/null', returnStdout: true).trim()

        if (!preGame) {
            if (pipelineParams.addonalDepends.size() > 0) {
                sh "sudo -E apt-get install -y ${pipelineParams.addonalDepends.join(' ')}"
            }
        }
    }

    def signPackages() {
        common.importGpgKey()

        def signCmd = []
        withFolderProperties {
            findFiles(glob: '*.deb').each {
                signCmd += "debsigs --sign=maint --default-key=${env.GPGKEY} ${it.name}"
            }
        }

        runShList(signCmd)
    }

    def packagesWithNewVersion() {
        def repoListName
        withFolderProperties {
            copyArtifacts(projectName: env.DEPLOY_JOB)
            repoListName = "${common.debianRelease}-${env.REPO_NAME}.list"
        }

        if (pipelineParams.playit) {
            def newPackages = []
            pipelineParams.playit_game_id.each {
                withEnv([ "PLAYIT_GAME_ID=${it}" ]) {
                    def packageName = sh(
                        script: '''
                            ARCHIVES_LIST=$(egrep 'ARCHIVE_[A-Z0-9_]*_VERSION' play-${PLAYIT_GAME_ID}.sh | cut -d = -f 1 | rev | cut -d _ -f 2- | rev | tr '\\n' ' ')
                            (
                                sed '/\\. "$PLAYIT_LIB2"/,$d' play-${PLAYIT_GAME_ID}.sh
                                echo
                                echo 'ARCHIVES_LIST=${ARCHIVES_LIST:-'"$ARCHIVES_LIST"'}'
                                echo 'ARCHIVES_TAB=($(echo $ARCHIVES_LIST))'
                                echo 'VERSION=$(eval echo -n \\$${ARCHIVES_TAB[0]}_VERSION)'
                                echo 'echo ${PKG_MAIN_ID:-$GAME_ID}_${VERSION}+${script_version}'
                            ) | bash
                        ''',
                        returnStdout: true
                    ).trim()

                    def updatePackage = sh(
                        script: "grep -q '^${packageName}' '${repoListName}'",
                        returnStatus: true
                    )

                    if (updatePackage != 0) {
                        newPackages << it
                    }
                }
            }

            pipelineParams.playit_game_id = newPackages
        }
    }

    def artifacts() {
        archiveArtifacts(artifacts: '*.deb', fingerprint: true, onlyIfSuccessful: true)
        artifactsSend = true
    }

    def post_step() {
        if (artifactsSend) {
            withFolderProperties {
                build(
                    job: env.DEPLOY_JOB,
                    parameters: [
                        string(name: 'sourceJob', value: env.JOB_NAME),
                        string(name: 'sourceJobNumber', value: env.BUILD_NUMBER),
                        string(name: 'repoName', value: env.REPO_NAME),
                        string(name: 'debianRelease', value: common.debianRelease),
                        booleanParam(name: 'forceAdd', value: pipelineParams.forceBuild)
                    ],
                    propagate: false,
                    wait: false
                )
            }
        }
    }
}
