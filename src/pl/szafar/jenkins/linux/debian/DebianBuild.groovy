package pl.szafar.jenkins.linux.debian

import javax.xml.xpath.*
import javax.xml.parsers.DocumentBuilderFactory
import pl.szafar.jenkins.linux.LinuxBuild

class DebianBuild extends LinuxBuild {
    def pkgList = null
    def firstBuild = true
    def artifactsSend = false
    def postDownloadCmd = []
    def preBuildCmd = []
    def postBuildCmd = []
    def buildDate = null

    DebianBuild(def jenkins) {
        super(jenkins)
        common = new DebianCommon(jenkins)
        pkgList = new DebianPackageList(jenkins)
    }

    def init(body) {
        pipelineParams += [
            ignorePackagesDeps: true,
            removeDepends: [],
            preDownloadCmds: [],
            preBuildCmd: [],
            postBuildCmd: [],
            gitBase: null,
            gitNames: [:],
            gitCloneSource: true,
            dpkg: true,
            reunpackFor32Bit: false,
            versionFromGitLatestTag: false,
            versionFromGitIgnore: null,
            versionFromGitRegExReplace: null,
            versionFromCmd: null,
            versionFromXPath: null,
            specFromTemplate: false,
            createChangelog: false,
            addRepo: [],
            sedDebianUseEnv: false,
            sedDebian: [:],
            sedRules: [],
            sedControl: [],
            sedDsc: [],
            debFromUrl: null
        ]

        triggerCron = 'H 3 * * *'
        triggerUrlCron = 'H */2 * * *'
        buildDate = new java.text.SimpleDateFormat('EEE, d MMM yyyy HH:mm:ss Z', Locale.US).format(currentBuild.startTimeInMillis)

        def argPipelineParams = super.init(body)

        preBuildAgentLabel += "(debian||docker)"
        buildAgentLabel += "(debian||docker)"

        if (!('control' in pipelineParams.sedDebian)) {
            pipelineParams.sedDebian['control'] = []
        }

        if (!('rules' in pipelineParams.sedDebian)) {
            pipelineParams.sedDebian['rules'] = []
        }

        pipelineParams.sedControl += pipelineParams.sedDsc
        pipelineParams.sedDebian['control'] += pipelineParams.sedControl
        pipelineParams.sedDebian['control.in'] = pipelineParams.sedDebian['control']
        pipelineParams.sedDebian['rules'] += pipelineParams.sedRules

        if (pipelineParams.debFromUrl instanceof String) {
            pipelineParams.debFromUrl = [ pipelineParams.debFromUrl, basename(pipelineParams.debFromUrl) ]
        }

        if (pipelineParams.addRepo instanceof Map) {
            pipelineParams.addRepo = [ pipelineParams.addRepo ]
        }

        preBuildCmd += pipelineParams.preBuildCmd
        postBuildCmd += pipelineParams.postBuildCmd

        dockerArgs = "-v /var/cache/apt/archives:/var/cache/apt/archives"

        withFolderProperties {
            if (!argPipelineParams.containsKey('dpkg')) {
                if (env.DPKG) {
                    pipelineParams.dpkg = env.DPKG.toBoolean()
                }
            }

            def envRepo = [:]
            if (env.ADD_REPO_REPO) {
                envRepo['repo'] = env.ADD_REPO_REPO.tokenize()
            }

            if (env.ADD_REPO_KEY) {
                envRepo['key'] = env.ADD_REPO_KEY
            }

            if (env.ADD_REPO_KEEP) {
                envRepo['keep'] = env.ADD_REPO_KEEP.toBoolean()
            }

            if (envRepo) {
                pipelineParams.addRepo += envRepo
            }

            if (pipelineParams.specFromTemplate) {
                if (!env.GIT_SPECS_URL) {
                    error("env GIT_SPECS_URL not set")
                }

                pipelineParams.createChangelog = true
            }

            if (pipelineParams.gitBase != null) {
                if (!argPipelineParams.containsKey('gitCloneSource')) {
                    if (pipelineParams.gitBase.indexOf('github.com') != -1) {
                        pipelineParams.gitCloneSource = false
                    }
                }
            }
            else {
                if (pipelineParams.versionFromGitLatestTag) {
                    error("gitBase must be set when versionFromGitLatestTag is true")
                }
            }
        }

        if (pipelineParams.versionFromXPath != null) {
            urlTriggerEntries += URLTriggerEntry(
                url: pipelineParams.versionFromXPath.url,
                timeout: 200,
                requestHeaders: [
                    RequestHeader( headerName: "Accept" , headerValue: "application/xml" )
                ],
                contentTypes: [
                    XMLContent([ XMLContentEntry( xPath: pipelineParams.versionFromXPath.xpath ) ])
                ]
            )
        }
    }

    def setVersionForAllPkgs(def version) {
        if (version.indexOf("-") == -1) {
            version = "${version}-1"
        }

        packagesToBuild.each {
            def pkg = pkgList[it]
            //if (pkg.version == null) {
                pkg.setVersionAndDeb(version)
                pkg.fullVersion = version
                pkg.sourceVersion = version
                
                pkg.base.setVersionAndDeb(version)
                pkg.base.fullVersion = version
                pkg.base.sourceVersion = version
            //}
        }
    }

    def prepare(boolean preBuild = false) {
        super.prepare(preBuild)

        if (!preBuild) {
            env.BUILD_NODE = env.NODE_NAME
        }
        env.BUILD_STEP = DebianBuildStep.PREPARE.toString()

        def firstRun = preBuild || (pipelineParams.forceBuild && firstBuild)
        def prepareCmd = []

        common.debianRelease = sh(script: 'lsb_release -cs 2> /dev/null', returnStdout: true).trim()
        common.is32Bit = (sh(script: 'dpkg-architecture -q DEB_BUILD_ARCH | grep -q i386', returnStatus: true) == 0)

        def addRepoCmd = []
        pipelineParams.addRepo.each {
            if ('deb' in it) {
                def repoDeb = basename(it['deb'])
                addRepoCmd += [
                    "wget -q ${it['deb']}",
                    "sudo -E apt-get install ./${repoDeb}",
                    "rm ${repoDeb}"
                ]
            }

            if ("key" in it) {
                addRepoCmd += "wget -q ${it['key']} -O - | gpg --dearmor | sudo -E tee /etc/apt/trusted.gpg.d/jenkins.gpg > /dev/null"
            }

            if ("repo" in it) {
                def sourcesListName = "jenkins"
                if (it.get('keep', false)) {
                    sourcesListName = "jenkins_keep"
                }

                addRepoCmd += "echo 'deb ${it['repo'].join(' ')}' | sudo -E tee -a /etc/apt/sources.list.d/${sourcesListName}.list"
                if (!pipelineParams.repackbuild) {
                    addRepoCmd += "echo 'deb-src ${it['repo'].join(' ')}' | sudo -E tee -a /etc/apt/sources.list.d/${sourcesListName}.list"
                }
            }

        }

        if (addRepoCmd) {
            addRepoCmd += [
                "sudo -E apt-get update"
            ]

            if (firstRun) {
                postDownloadCmd += "sudo -E sh -c 'rm /etc/apt/sources.list.d/jenkins.list && apt-get update' || true"
                postBuildCmd += [
                    "sudo -E rm -f /etc/apt/sources.list.d/jenkins.list",
                    "sudo -E rm -f /etc/apt/sources.list.d/jenkins_keep.list",
                    "sudo -E rm -f /etc/apt/trusted.gpg.d/jenkins.gpg"
                ]
            }

            runShList(addRepoCmd)
        }

        if (firstRun) {
            if (pipelineParams.add32Bit) {
                runShList([
                    "sudo -E dpkg --add-architecture i386",
                    "sudo -E apt-get update"
                ])

                postBuildCmd += [
                    "sudo -E apt-get purge --allow-remove-essential -y \\*:i386 > /dev/null || true",
                    "sudo -E dpkg --remove-architecture i386"
                ]
            }


            if (pipelineParams.dpkg) {
                if (pipelineParams.withDeps) {
                    packagesToBuild += pkgList.getDepsOfPackage(packagesToBuild)
                }

                def packagesWildcard = pkgList.findWildcardArg(packagesToBuild)
                def ignoreWildcard = pkgList.findWildcardArg(pipelineParams.ignorePackages)
                def packagesFromRegex = pkgList.findWildcardPackages(packagesWildcard)
                def ignoreFromRegex = pkgList.findWildcardPackages(ignoreWildcard)

                packagesToBuild -= packagesWildcard
                packagesToBuild += packagesFromRegex

                pkgList.updatePackageInfo(packagesToBuild)

                pipelineParams.ignorePackages += ignoreFromRegex

                pipelineParams.ignorePackages = pkgList.getAllPackagesWithSameBase(pipelineParams.ignorePackages)
                packagesToBuild = pkgList.removePackagesWithSameSource(packagesToBuild)

                if (!pipelineParams.repackbuild) {
                    pkgList.getAllPackagesWithSameBase(packagesToBuild).each {
                        if (it in pkgList) {
                            def pkg = pkgList[it]
                            pkg.setVersionAndDeb(DebianPackage.versionsSlightlyHigher(pkg.version))

                            if (pkg.base.virtual) {
                                pkg.base.version = DebianPackage.compareVersion(pkg.base.version, pkg.version)
                                pkg.base.fullVersion = DebianPackage.compareVersion(pkg.base.fullVersion, pkg.fullVersion)
                            }
                        }
                    }

                    packagesToBuild.each {
                        def pkg = pkgList[it]
                        if (pkg.base.virtual) {
                            pkg.setVersionAndDeb(DebianPackage.compareVersion(pkg.base.version, pkg.version))
                        }
                    }
                }
            }
            else {
                packagesToBuild.each {
                    def pkg = new DebianPackage(it)
                    if (it in pipelineParams.gitNames) {
                        pkg.gitName = pipelineParams.gitNames[it]
                    }

                    pkgList[it] = pkg
                }
            }

            packagesToBuild -= pipelineParams.ignorePackages

            env.DEBIAN_RELEASE = common.debianRelease
            env.PACKAGE_TO_BUILD = packagesToBuild.join(',')

            if (pipelineParams.versionFromGitLatestTag) {
                packagesToBuild.each {
                    def pkg = pkgList[it]
                    def gitTags = sh(
                        script: "git ls-remote --tags --refs '${pipelineParams.gitBase}/${pkg.gitName}.git' | cut -d '/' -f 3- | sort -V",
                        returnStdout: true
                    ).trim().tokenize()

                    gitTags.reverse().any { gitTag ->
                        if (pipelineParams.versionFromGitIgnore) {
                            if (gitTag =~ pipelineParams.versionFromGitIgnore) {
                                return false // continue
                            }
                        }

                        def newversion = gitTag.replaceAll(/^[^0-9]*/, "")

                        if (pipelineParams.versionFromGitRegExReplace) {
                            newversion = newversion.replaceAll(pipelineParams.versionFromGitRegExReplace[0], pipelineParams.versionFromGitRegExReplace[1])
                        }

                        if ((!newversion) || (gitTag.indexOf('alpha') != -1) || (gitTag.indexOf('beta') != -1) || (gitTag.indexOf('nightly') != -1)) {
                            return false // continue
                        }

                        pkg.version = DebianPackage.versionsSlightlyHigher(newversion)
                        pkg.gitTag = gitTag

                        return true // break
                    }
                }
            }

            if (pipelineParams.versionFromXPath != null) {
                def xpath = XPathFactory.newInstance().newXPath()
                def builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()

                def inputStream = new URL(pipelineParams.versionFromXPath.url).openConnection().getInputStream();
                def records = builder.parse(inputStream).documentElement
                def version = xpath.evaluate( pipelineParams.versionFromXPath.xpath, records ).trim()

                pipelineParams.versionFromXPath.replace.each {
                    version = version.replaceAll(it[0], it[1])
                }
                if (version == "") {
                    error("Cannot find version from XPath: ${pipelineParams.versionFromXPath.xpath}")
                }

                setVersionForAllPkgs(version)
            }

            if (pipelineParams.versionFromCmd != null) {
                def version = ""
                dir(env.TMPDIR) {
                    version = sh(
                        script: pipelineParams.versionFromCmd,
                        returnStdout: true
                    ).trim()
                }

                if (version == "") {
                    error("Cannot find version from cmd: ${pipelineParams.versionFromCmd}")
                }

                setVersionForAllPkgs(version)
            }
        }

        if (pipelineParams.debFromUrl) {
            packagesToBuild.each {
                def pkg = pkgList[it]
                pkg.deb = expandString(pipelineParams.debFromUrl[1], [ pkg: pkg ])
                pipelineParams.sedDebian['control'] += "s/\\(Version:\\).*/\\1 ${pkg.version}/"
            }
        }

        def pkgNotFound = packagesToBuild.findAll { !(it in pkgList) }
        if (pkgNotFound) {
            error("Packages not found in database: ${pkgNotFound.join(', ')}")
        }

        if (!preBuild) {
            if (pipelineParams.add32Bit) {
                if (firstBuild) {
                    pipelineParams.sedDebian['changelog'] = [ "s/\\(${env.DEBFULLNAME} <${env.DEBEMAIL}>\\).*/\\1  ${buildDate}/" ]

                    if (common.is32Bit) {
                        pipelineParams.addonalDepends.clone().each {
                            def newDep = it.replace('amd64', 'i386')
                            if (it != newDep) {
                                pipelineParams.addonalDepends -= it
                                pipelineParams.addonalDepends += newDep
                            }
                         }
                    }
                }

                if (!isAgentRunningDocker()) {
                    if (pipelineParams.addonalDepends && pipelineParams.dpkg) {
                        pipelineParams.sedDsc += [ "s/Build-Depends:/& ${pipelineParams.addonalDepends.join(', ')},/" ]
                        pipelineParams.addonalDepends = []
                    }

                    pipelineParams.addonalDepends += [ "pbuilder" ]
                }
            }

            prepareCmd += shCwd(env.BUILDDIR, [
                "rm -rf ${packagesToBuild.join(' ')}",
                "mkdir ${packagesToBuild.join(' ')}"
            ])
        }

        runShList(prepareCmd)
    }

    def download(boolean preBuild = false) {
        env.BUILD_STEP = DebianBuildStep.DOWNLOAD.toString()
        super.download(preBuild)

        if (!preBuild) {
            dir(env.BUILDDIR) {
                downloadSources()
            }
        }
    }

    def preBuild() {
        env.BUILD_STEP = DebianBuildStep.PREBUILD.toString()
        super.preBuild()

        dir(env.BUILDDIR) {
            packagesWithNewVersion()
        }
    }

    def extract() {
        env.BUILD_STEP = DebianBuildStep.EXTRACT.toString()
        super.extract()

        dir(env.BUILDDIR) {
            def extractCmd = []

            packagesToBuild.each {
                def pkg = pkgList[it]
                extractCmd += shCwd(pkg.name,
                    extractPkgCmd(pkg) + patchPkgCmd(pkg)
                )
            }

            runShList(extractCmd)
        }
    }

    def building() {
        super.building()

        dir(env.BUILDDIR) {
            installDeps()

            if (!pipelineParams.repackbuild) {
                applyCompilationFlags()
            }
            buildSources()
        }
    }

    def downloadSources() {
        def downloadPreCmd = []
        def downloadCmd = []
        def downloadPostCmd = []
        downloadPreCmd += pipelineParams.preDownloadCmds
        downloadPostCmd += this.postDownloadCmd

        pipelineParams.addonalDepends.clone().each {
            if (it.startsWith("http://") || it.startsWith("https://")) {
                def depBasename = basename(it)
                downloadCmd += shCwd(env.TMPDIR, [
                    "wget -q ${it}",
                    "echo 'Downloaded ${depBasename}'",
                ])
                pipelineParams.addonalDepends += "./${depBasename}"
                pipelineParams.addonalDepends -= it
            }
        }

        if (pipelineParams.repackbuild) {
            if (pipelineParams.debFromUrl != null) {
                packagesToBuild.each {
                    def pkg = pkgList[it]
                    def url = expandString(pipelineParams.debFromUrl[0], [ pkg: pkg ])
                    downloadCmd += shCwd(pkg.name, "wget -q ${url} -O ${pkg.deb}")
                }
            }

            if (pipelineParams.dpkg) {
                packagesToBuild.each {
                    def pkg = pkgList[it]
                    downloadCmd += shCwd(pkg.name, "apt-get download ${pkg.name}=${pkg.fullVersion}")
                }
            }
        }
        else {
            if (pipelineParams.gitBase != null) {
                packagesToBuild.each {
                    def pkg = pkgList[it]
                    def gitCmd = []

                    if (pipelineParams.gitCloneSource) {
                        gitCmd += [
                            "git",
                                "clone",
                                "--quiet",
                                "--single-branch",
                                "--recurse-submodules",
                                "--branch", pkg.gitTag,
                                "${pipelineParams.gitBase}/${pkg.gitName}.git",
                                "build"
                        ]
                    }
                    else {
                        if (pipelineParams.gitBase.indexOf('github.com') != -1) {
                            gitCmd += [
                                "wget",
                                    "--quiet",
                                    "${pipelineParams.gitBase}/${pkg.gitName}/archive/refs/tags/${pkg.gitTag}.tar.gz"
                            ]
                        }
                        else {
                            error("Cannot download source archive from ${pipelineParams.gitBase}/${pkg.gitName}")
                        }
                    }

                    downloadCmd += shCwd(pkg.name, [
                        gitCmd.join(' '),
                        "echo 'Downloaded ${pkg.name} (source)'"
                    ])
                }
            }

            if (pipelineParams.specFromTemplate) {
                packagesToBuild.collect {
                    def pkg = pkgList[it]
                    downloadCmd += shCwd(pkg.name, "cp -a '${env.WORKSPACE}/specs/${pkg.name}' ./debian")
                }
            }

            if (pipelineParams.dpkg) {
                def sedDscCmd = []
                if (pipelineParams.sedDsc) {
                    sedDscCmd += "sed -i *.dsc -e '${pipelineParams.sedDsc.join("' -e '")}'"
                }

                packagesToBuild.each {
                    def pkg = pkgList[it]
                    def downloadAptCmd = [
                        "apt-get source --download-only -qq ${pkg.name}=${pkg.sourceVersion}",
                        "echo 'Downloaded ${pkg.name} (source)'"
                    ]
                    downloadCmd += shCwd(pkg.name, downloadAptCmd + sedDscCmd)
                }
            }
        }

        if (pipelineParams.manualDownload) {
            if (isStartedByUser) {
                notify("Waiting ${currentBuild.projectName}", "Now copy the files for ${currentBuild.fullProjectName}")
                input(id: 'userInput', message: 'Now copy the files')
            }
            else {
                error('Manual download requested')
            }
        }

        runShList(
            downloadPreCmd +
            [ shParallel(downloadCmd) ] +
            downloadPostCmd
        )

        findFiles().each {
            def dscList = findFiles(glob: "${it.name}/*.dsc")
            if (dscList) {
                if (dscList.size() > 1) {
                    error("More than one dsc files (${dscList.join(', ')})")
                }

                def pkg = pkgList[it.name]
                pkg.dsc = dscList[0].name
            }
        }
    }

    def installDeps() {
        def installDepCmd = []
        def installDepList = []
        def builddepList = []
        
        if (!pipelineParams.dpkg && !pipelineParams.repackbuild) {
            def mkBuildDepsCmd = []
            packagesToBuild.collect {
                def pkg = pkgList[it]
                mkBuildDepsCmd += shCwd("${pkg.name}/build", "mk-build-deps")
            }

            runShList([ shParallel(mkBuildDepsCmd) ])
            dir(env.TMPDIR) {
                installDepList += findFiles(glob: "*-build-deps_*.deb").collect { "./${it.name}" }
            }
        }

        installDepList += pipelineParams.addonalDepends
    
        packagesToBuild.each  {
            def pkg = pkgList[it]
            if (pkg.dsc) {
                builddepList += "./${pkg.name}/${pkg.dsc}"
            }
        }

        if (installDepList) {
            installDepCmd += shCwd(env.TMPDIR, "sudo -E apt-get install -y ${installDepList.join(' ')}")

            def depPackages = installDepList.findAll { it.startsWith("./") }
            if (depPackages) {
                installDepCmd += shCwd(env.TMPDIR, "rm ${depPackages.join(' ')}")
            }
        }

        if (builddepList) {
            installDepCmd += "sudo -E apt-get build-dep -y ${builddepList.join(' ')}"
        }

        if (pipelineParams.removeDepends) {
            installDepCmd += "sudo -E apt-get purge -y ${pipelineParams.removeDepends.join(' ')}"
        }

        if (pipelineParams.add32Bit && !isAgentRunningDocker()) {
            installDepCmd += "sudo -E pbuilder --create --distribution ${common.debianRelease} --architecture i386 --basetgz '${env.TMPDIR}/pbuilder.tgz'"
        }

        runShList(installDepCmd)
    }

    def extractPkgCmd(def pkg) {
        def extractCmd = []

        if (pipelineParams.repackbuild) {
            extractCmd += joinSh([
                "dpkg-deb -R ${pkg.deb} build",
                "rm ${pkg.deb}"
            ], true)
        }
        else if (pkg.dsc != null) {
            def extractDscCmd = [ "dpkg-source -x ${pkg.dsc} build" ]
            extractDscCmd += shCwd("build", [
                    "dch --newversion ${DebianPackage.versionsSlightlyHigher(pkg.base.fullVersion)} --force-bad-version --package ${pkg.base.name} 'Szafar rebuild'",
                    "dch --release 'Szafar rebuild'"
                ]
            )

            extractCmd += joinSh(extractDscCmd, true)
        }
        else if ((pipelineParams.gitBase != null) && (!pipelineParams.gitCloneSource)) {
            if (pipelineParams.gitBase.indexOf('github.com') != -1) {
                extractCmd += joinSh([
                    "tar -xzf ${pkg.gitTag}.tar.gz",
                    "mv ${pkg.gitName}-* build",
                    "rm ${pkg.gitTag}.tar.gz",
                ], true)
            }
        }


        if (pipelineParams.specFromTemplate) {
            extractCmd += "mv debian build/"
        }

        if (pipelineParams.createChangelog) {
            extractCmd += shCwd("build", [
                "rm -f debian/changelog",
                "dch --create --newversion ${pkg.version} --package ${pkg.base.name} 'New upstream release'",
                "dch --release 'New upstream release'"
            ])
        }



        return extractCmd
    }

    def patchPkgCmd(def pkg) {
        def patchCmd = []
        def patchDirPath = "${env.WORKSPACE}/patch/${pkg.base.name}"
        def debianDir = pipelineParams.repackbuild ? "DEBIAN" : "debian"

        if (fileExists(patchDirPath)) {
            dir(patchDirPath) {
                patchCmd += "mkdir -p patches/"
                findFiles(glob: "*.patch").each {
                    if (it.name == "rules.patch") {
                        patchCmd += "patch -p1 -i '${patchDirPath}/rules.patch'"
                    }
                    else {
                        patchCmd += [
                            "cp '${patchDirPath}/${it.name}' patches/",
                            "echo '${it.name}' >> patches/series"
                        ]
                    }
                }
            }
        }

        pipelineParams.sedDebian.each { sedFile, sedExpr ->
            if (sedExpr) {
                if (pipelineParams.sedDebianUseEnv) {
                    sedExpr = sedExpr.collect { expandString(it, [env: env]) }
                }

                patchCmd += "( [ ! -f '${sedFile}' ] || sed -i '${sedFile}' -e '${sedExpr.join("' -e '")}' )"
            }
        }
        
        if (patchCmd) {
            return [ shCwd("build/${debianDir}", patchCmd) ]
        }
        else {
            return []
        }
    }

    def applyCompilationFlags() {
        if (fileExists("${env.WORKSPACE}/compilation-flags")) {
            sh 'ls $WORKSPACE/compilation-flags/sets/* | xargs sed "s/#.*//" > "$TMPDIR/compilationSets"'
            packagesToBuild.each {
                def pkg = pkgList[it]
                def specialCflagsSets = sh(
                    script: """
                        awk 'match("${pkg.base.name}", "^"\$1"\$") { for (i = 2; i <= NF; ++i) print \$i; exit }' "${env.TMPDIR}/compilationSets"
                    """,
                    returnStdout: true
                ).trim().tokenize()

                pkg.preBuildCmd += ". ${env.WORKSPACE}/compilation-flags/global"

                if (specialCflagsSets) {
                    echo("Special CFLAGS sets for ${pkg.name}: ${specialCflagsSets.join(' ')}")

                    specialCflagsSets.each { cflagsSet ->
                        if (cflagsSet.endsWith('-32')) {
                            if (common.is32Bit) {
                                cflagsSet = cflagsSet.replace('-32', '')
                                echo("Use ${cflagsSet}")
                            }
                            else {
                                echo("Ignoring ${cflagsSet} only for 32-bit")
                                return // continue
                            }
                        }

                        if (cflagsSet.startsWith('no-')) {
                            def flagName = cflagsSet.substring("no-".length()).replace('-', '_')
                            [ 'CFLAGS', 'LDFLAGS', 'RUSTFLAGS' ].each { baseFlag ->
                                def envFlag = "\$${baseFlag}_${flagName}"
                                def flagSep = baseFlag == "LDFLAGS" ? ',' : ' '
                                pkg.preBuildCmd += """[ -z "${envFlag}" ] || ${baseFlag}=`echo \$${baseFlag} | sed "s/${flagSep}${envFlag}//"`"""
                            }
                        }
                        else if (cflagsSet.startsWith('only-')) {
                            def flagName = cflagsSet.substring("only-".length()).replace('-', '_')
                            [ 'CFLAGS', 'LDFLAGS', 'RUSTFLAGS' ].each { baseFlag ->
                                def envFlag = "\$${baseFlag}_${flagName}"
                                def flagSep = baseFlag == "LDFLAGS" ? ',' : ' '
                                pkg.preBuildCmd += """[ -z "${envFlag}" ] || ${baseFlag}="\$${baseFlag}_base${flagSep}${envFlag}" """
                            }
                        }
                        else {
                            def flagName = cflagsSet.replace('-', '_')
                            [ 'CFLAGS', 'LDFLAGS', 'RUSTFLAGS' ].each { baseFlag ->
                                def envFlag = "\$${baseFlag}_${flagName}"
                                def flagSep = baseFlag == "LDFLAGS" ? ',' : ' '
                                pkg.preBuildCmd += """[ -z "${envFlag}" ] || ${baseFlag}="\$${baseFlag}${flagSep}${envFlag}" """
                            }
                        }
                    }
                }

                pkg.preBuildCmd += 'RUST="$RUSTFLAGS"'
                [ 'CFLAGS', 'LDFLAGS', 'RUST', 'RUSTFLAGS' ].each {
                    pkg.preBuildCmd += "export DEB_${it}_SET=\"\$${it}\""
                }
                [ 'CXXFLAGS', 'FFLAGS', 'FCFLAGS' ].each {
                    pkg.preBuildCmd += "export DEB_${it}_SET=\"\$CFLAGS\""
                }
            }
        }
    }

    def buildSources() {
        runShList(preBuildCmd)
        def errorList = []

        packagesToBuild.each {
            env.BUILD_STEP = DebianBuildStep.BUILD.toString()
            def pkg = pkgList[it]
            def buildCmd = []

            withFolderProperties {
                buildCmd += pkg.preBuildCmd
                if (pipelineParams.repackbuild) {
                    buildCmd += [
                        "fakeroot dpkg-deb --build build ${pkg.deb}",
                        "debsigs --sign=maint --default-key=${env.GPGKEY} ${pkg.deb}"
                    ]
                } else {
                    buildCmd += shCwd("build", "debuild -b -k${env.GPGKEY}")

                    if (pipelineParams.add32Bit && !isAgentRunningDocker()) {
                        if (pipelineParams.reunpackFor32Bit) {
                            buildCmd += "rm -rf build"
                            buildCmd += extractPkgCmd(pkg)
                            buildCmd += patchPkgCmd(pkg)
                        }

                        buildCmd += shCwd("build",
                            "pdebuild --debsign-k ${env.GPGKEY} --buildresult ${env.WORKSPACE}/build/${pkg.name} -- --distribution ${common.debianRelease} --architecture i386 --basetgz ${env.TMPDIR}/pbuilder.tgz"
                        )
                    }
                }

                buildCmd += pkg.postBuildCmd
                pkg.preBuildCmd.clear()
                pkg.postBuildCmd.clear()
            }

            dir(pkg.name) {
                echo("### Building: ${pkg.base.name} ###")
                def result = sh(
                    script: joinSh(setNiceCmd + buildCmd),
                    returnStatus: true
                )

                if (result == 0) {
                    /*if (pipelineParams.dpkg) {
                        correctPackagesName()
                    }*/
                    archiveArtifacts(artifacts: '*.deb', fingerprint: true, onlyIfSuccessful: false)
                    artifactsSend = true
                }
                else {
                    errorList += pkg.base.name
                }
            }
        }

        if (errorList) {
            error("Error when building: ${errorList.join(", ")}")
        }

        firstBuild = false
        env.BUILD_STEP = DebianBuildStep.CLEANUP.toString()
    }

    def correctPackagesName() {
        def correctCmd = []
        findFiles(glob: "*.deb").each {
            def filepkgTab = it.name.split('_')
            def filepkg = filepkgTab[0]
            def fileVersion = filepkgTab[1]
            def fileSuffix = filepkgTab[2]

            def pkg = pkgList[filepkg]

            if (fileVersion != pkg.version) {
                def pkgdestname = "${filepkg}_${pkg.version}_${fileSuffix}"
                correctCmd += "mv '${it.name}' '${pkgdestname}'"
            }
        }

        runShList(correctCmd)
    }

    def packagesWithNewVersion() {
        def newPackages = []
        def repoListName
        withFolderProperties {
            copyArtifacts(projectName: env.DEPLOY_JOB)
            repoListName = "${common.debianRelease}-${env.REPO_NAME}.list"
        }

        packagesToBuild.each {
            def pkg = pkgList[it]
            def pkgDeb = pkg.deb

            if (pkgDeb == null) {
                pkgDeb = "${pkg.name}_${pkg.version}_"
            }

            def updatePackage = sh(
                script: "grep -q '${pkgDeb}' '${repoListName}'",
                returnStatus: true
            )

            if (updatePackage != 0) {
                newPackages += it
            }
        }

        packagesToBuild = newPackages
        pipelineParams.withDeps = false
    }

    def post_step() {
        if (artifactsSend) {
            withFolderProperties {
                build(
                    job: env.DEPLOY_JOB,
                    parameters: [
                        string(name: 'sourceJob', value: env.JOB_NAME),
                        string(name: 'sourceJobNumber', value: env.BUILD_NUMBER),
                        string(name: 'repoName', value: env.REPO_NAME),
                        string(name: 'debianRelease', value: common.debianRelease),
                        booleanParam(name: 'forceAdd', value: pipelineParams.forceBuild)
                    ],
                    propagate: false,
                    wait: false
                )
            }
        }
    }

    def cleanup_step() {
        if (!isAgentRunningDocker()) {
            runShList(postBuildCmd)
        }
        super.cleanup_step()
    }
}
