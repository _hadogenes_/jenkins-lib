package pl.szafar.jenkins.linux.debian;

public enum DebianBuildStep {
    INIT,
    PREPARE,
    DOWNLOAD,
    PREBUILD,
    EXTRACT,
    BUILD,
    ARTIFACTS,
    CLEANUP
}
