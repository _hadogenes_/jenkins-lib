package pl.szafar.jenkins.linux.debian

import pl.szafar.jenkins.CommonTools

class DebianPackageList extends CommonTools {
    private def pkgList = null

    DebianPackageList(def jenkins) {
        super(jenkins)
        pkgList = [:]
    }
    
    def findWildcardArg(def packageList) {
        return packageList.findAll { (it.indexOf('*') != -1) || it.indexOf('[') != -1 }
    }

     def getDepsOfPackage(def packagesList) {
        def deps = []
        if (packagesList.size() > 0) {
            def depsList = sh(
                script: """
                    apt-cache show ${packagesList.join(' ')} 2> /dev/null |\
                        awk '\$1 == "Depends:"{ print \$1 }'
                """,
                returnStdout: true
            ).trim().tokenize('\n')

            depsList.each { dlist ->
                dlist.split(', ').each {
                    deps += it.tokenize()[0]
                }
            }
        }

        return deps
    }

    def findWildcardPackages(def packagesWildcard) {
        def packagesFromRegex = []
        if (packagesWildcard.size() > 0) {
            packagesFromRegex += sh(
                script: """
                    apt-cache show ${packagesWildcard.join(' ')} |\
                        awk '\$1 == "Package:" { print \$2 }'
                """,
                returnStdout: true
            ).trim().tokenize()
        }

        return packagesFromRegex
    }

    def getAllPackagesWithSameBase(def packagesList) {
        def fullpackagesList = []

        if (packagesList.size() > 0) {
            fullpackagesList += sh(
                script: """
                    apt-cache showsrc ${packagesList.join(' ')} |\
                        tr '\\n' '\\f' | sed 's/\\f //g' | tr '\\f' '\\n' |\
                        awk '\$1 == "Binary:" { for (i = 2; i <= NF; ++i) print \$i }' |\
                        tr ',' '\\n' |\
                        grep -v '^\$' || true
                """,
                returnStdout: true
            ).trim().tokenize().unique(false)
        }
        
        return fullpackagesList
    }

    def updatePackageInfo(def packagesToBuild) {
        def fullPackagesToBuild = getAllPackagesWithSameBase(packagesToBuild)
        fullPackagesToBuild += packagesToBuild

        def pkgToUpdate = fullPackagesToBuild.findAll { !(it in pkgList) }

        if (pkgToUpdate.size() > 0) {
            pkgToUpdate.unique()
            def sourcePackageList = sh(
                script: """
                    apt-cache show ${pkgToUpdate.join(' ')} |\
                        awk '\$1 == "Package:" { name=\$2 } \$1 == "Source:" { source=\$2 } \$1 == "Version:" { version=\$2 } \$1 == "Filename:" { print name";"version";"source";"\$2; name=version=source="";  }'
                """,
                returnStdout: true
            ).trim().tokenize()

            sourcePackageList.each {
                def valueList = it.split(';')
                def packageName = valueList[0]
                def fullVersion = valueList[1]
                def version = fullVersion
                def sourceVersion = version.split('\\+b')[0]
                def sourceName = valueList[2] ? valueList[2] : packageName
                def packageFile = basename(valueList[3])

                def destpkgTab = packageFile.split('_')
                if (destpkgTab.size() == 3) {
                    version = destpkgTab[1]
                }


                packageFile = "${packageName}_${version}_${destpkgTab[destpkgTab.length - 1]}"

                if (packageName in pkgList) {
                    def pkg = pkgList[packageName]
                    pkg.virtual = false

                    if (pkg.fullVersion == fullVersion) { // pkg created when source package is needed
                        pkg.deb = packageFile
                    }
                    else if (DebianPackage.compareVersion(pkg.fullVersion, fullVersion) == fullVersion) {
                        pkg.version = version
                        pkg.fullVersion = fullVersion
                        pkg.deb = packageFile

                        if (pkg.base.virtual) {
                            pkg.base.version = version
                            pkg.base.fullVersion = fullVersion
                            pkg.base.sourceVersion = sourceVersion
                        }
                    }
                }
                else {
                    def pkg = new DebianPackage(packageName, packageFile, fullVersion, sourceVersion, version)
                    pkgList[packageName] = pkg
                    //pkgList["${packageName}:i386"] = pkg
                    pkgList["${packageName}-dbgsym"] = pkg

                    if (sourceName != packageName) {
                        if (sourceName in pkgList) {
                            pkg.base = pkgList[sourceName]
                        }
                        else {
                            pkg.base = new DebianPackage(sourceName, null, fullVersion, sourceVersion, version)
                            pkg.base.virtual = true
                            pkgList[sourceName] = pkg.base
                            //pkgList["${packageName}:i386"] = pkg
                            pkgList["${sourceName}-dbgsym"] = pkg.base
                        }
                    }
                }
            }
        }
    }
    
    def removePackagesWithSameSource(def packagesList) {
        def newPackages = []
        def packagesBase = []
        packagesList.each {
            def pkg = pkgList[it]
            if (!(pkg.base in packagesBase)) {
                newPackages += it
                packagesBase += pkg.base
            }
        }

        return newPackages
    }

    DebianPackage getAt(String name) {
        if (!(name in pkgList)) {
            error("Pkg '${name}' not found in pkgList")
        }
        return pkgList[name]
    }
    
    def putAt(String name, DebianPackage pkg) {
        return pkgList[name] = pkg
    }
    
    boolean isCase(String name) {
        return name in pkgList
    }
}
