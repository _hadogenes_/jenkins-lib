package pl.szafar.jenkins.linux.debian

import pl.szafar.jenkins.linux.LinuxCommon

class DebianCommon extends LinuxCommon {
    def debianRelease = null

    DebianCommon(def jenkins) {
        super(jenkins)
    }

    def systemUpdate(boolean isAgentRunningDocker, boolean upgradeSystem = true) {
        def updateCmd = [
            "sudo -E dpkg --configure -a || true",
            "sudo -E apt-get update"
        ]

        if (upgradeSystem) {
            updateCmd += "sudo -E apt-get upgrade -y"
        }

        runShList(updateCmd)
    }

    def setupEnv() {
        super.setupEnv()
        env.DH_QUIET=1
        env.DH_COLORS="always"
        env.DPKG_COLORS="always"
        env.DPKG_DEB_THREADS_MAX=env.THREAD_NUM
        env.DEB_BUILD_OPTIONS="nocheck terse parallel=${env.THREAD_NUM.toInteger() + 1}"
        env.DEBIAN_FRONTEND="noninteractive"
    }

//    def addCustomRepo() {
//         withFolderProperties {
//             if (env.REPO_URL) {
//                 sh '''
//                     (
//                         echo "[$REPO_NAME]"
//                         echo "SigLevel = Required"
//                         echo "Server = $REPO_URL"
//                     ) | sudo -E tee -a /etc/pacman.conf
// 
//                     sudo -E pacman -Sy
//                 '''
//             }
//         }
//    }

//      def getTools(boolean preBuild = false) {
//          super.getTools(preBuild)
// 
//          withFolderProperties {
//             if (env.GIT_SPECS_URL) {
//                 dir("specs") {
//                     git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_SPECS_URL
//                 }
//             }
//         }
//      }

    def importGpgKey() {
        super.importGpgKey()
        def packager = sh(
            script: '''
                gpg --import --import-options show-only --with-colons public-key.gpg | awk -F ':' '$1 == "uid" { print $10 }'
            ''',
            returnStdout: true
        ).trim()
        
        def (_, packagerName, packagerComment, packagerEmail) = (packager =~ /([^\(]*)( \(.*\))? <(.*)>/)[0]

        env.DEBFULLNAME = packagerName
        env.DEBEMAIL = packagerEmail
    }

    def cleanup(boolean isAgentRunningDocker = false) {
        def cleanupCmd = []

        if (!isAgentRunningDocker) {
            def requiredPkgs= [ "default-jre", "git", "openssh-server", "parallel", "gawk", "wget ","sudo", "gpg", "jq", "ifupdown", "lsb-release", "build-essential", "devscripts", "docker.io", "debsigs", "lgogdownloader", "play.it" ]

            cleanupCmd += [
                "sudo -E dpkg --configure -a || true",
                "sudo -E apt-get install -y ${requiredPkgs.join(' ')} > /dev/null || true",
                "sudo -E apt-mark auto \\* > /dev/null || true",
                "sudo -E apt-mark manual ${requiredPkgs.join(' ')} > /dev/null || true",
                "sudo -E apt-get autoremove -y --purge > /dev/null",
            ]

            if (debianRelease) {
                cleanupCmd += [
                    """(
                        echo 'Package: *'
                        echo 'Pin: release n=${debianRelease}'
                        echo 'Pin-Priority: 1001'
                    ) | sudo tee /etc/apt/preferences.d/force > /dev/null""",
                    "sudo apt-get upgrade --allow-downgrades -y > /dev/null",
                    "sudo rm -f /etc/apt/preferences.d/force",
                    "sudo -E apt-get autoremove -y --purge > /dev/null",
                ]
            }
        }

        runShList(cleanupCmd)

        super.cleanup(isAgentRunningDocker)
    }
}
