package pl.szafar.jenkins.linux

import pl.szafar.jenkins.DockerStep

abstract class LinuxGame extends DockerStep {
    def common
    def gameLogin
    def playitSystemPath = "/usr/share/play.it"
    def preBuildAgentLabel = [ 'x86', 'prebuild' ]
    def buildAgentLabel = [ 'x86', 'gamebuild' ]
    def artifactsSend = false
    def compressionMethod = null

    def pipelineParams = [
        forceBuild: false,
        gog: false,
        gog_game_id: [ currentBuild.projectName.replaceAll('-', '_') ],
        gog_game_include: [ 'installers' , 'languagepacks' ,'dlcs' ],
        gog_game_exclude: [],
        gog_platform: [ 'linux', 'windows' ],
        playit: true,
        playit_current: true,
        playit_sedScript: [:],
        playit_game_id: [ currentBuild.projectName ],
        playit_merge_requests_gmaes: [],
        playit_merge_requests_scripts: [],
        downloadExtraUrl: [:],
        addonalDepends: [],
        addonalPreBuildLabel: [],
        addonalBuildLabel: [],
    ]

    LinuxGame(jenkins) {
        super(jenkins)
        gameLogin = new LinuxGameLogin(jenkins)
    }

    def init(body) {
        def argPipelineParams = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = argPipelineParams
        body()

        pipelineParams << argPipelineParams

        if (pipelineParams.addonalPreBuildLabel instanceof String) {
            pipelineParams.addonalPreBuildLabel = [ pipelineParams.addonalPreBuildLabel ]
        }

        if (pipelineParams.addonalBuildLabel instanceof String) {
            pipelineParams.addonalBuildLabel = [ pipelineParams.addonalBuildLabel ]
        }

        if (pipelineParams.gog_game_id instanceof String) {
            pipelineParams.gog_game_id = [ pipelineParams.gog_game_id ]
        }

        if (pipelineParams.playit_game_id instanceof String) {
            pipelineParams.playit_game_id = [ pipelineParams.playit_game_id ]
        }

        preBuildAgentLabel += pipelineParams.addonalPreBuildLabel
        buildAgentLabel += pipelineParams.addonalBuildLabel

        withFolderProperties {
            dockerImage += env.DOCKER_IMAGE

            if (env.COMPRESION_METHOD) {
                compressionMethod = env.COMPRESION_METHOD
            }
        }

        if (pipelineParams.playit_current) {
            if (compressionMethod == "xz") {
                compressionMethod = "size"
            }
        }
    }

    def pregame_step() {
        runStep {
            prepare(true)
            prepareScripts()
            patchScripts()
            checkGameUpdated()
            packagesWithNewVersion()
        }

        if (pipelineParams.playit_game_id.size() > 0) {
            echo "Packages to build: ${pipelineParams.playit_game_id.join(" ")}"
            notify("Building ${currentBuild.projectName}", "Packages to build: ${pipelineParams.playit_game_id.join(" ")}")
        }
        else {
            echo "Nothing to build"
        }
    }

    def game_step() {
        runStep {
            prepare(false)
            prepareScripts()
            patchScripts()
            download()
            pack()
            signPackages()
            artifacts()
        }
    }

    def cleanup_step() {
        common.cleanup(isAgentRunningDocker())
    }

    def prepare(boolean preGame = false) {
        common.getTools(preGame)

        if (pipelineParams.gog) {
            gameLogin.gog()
        }
    }

    def prepareScripts() {
        def preparePreCmd = []
        def prepareCmd = []
        def preparePostCmd = []

        if (pipelineParams.playit) {
            def playitPath = playitSystemPath

            if (pipelineParams.playit_current) {
                playitPath = "${env.WORKSPACE}/playit"

                preparePreCmd += "mkdir -p ${playitPath}/games"
                prepareCmd += "wget -q -O '${playitPath}/libplayit2.sh' https://downloads.dotslashplay.it/releases/current/libplayit2.sh"

                [ 'vv221/40_vv221-games', 'adventure/45_adventure', 'puzzle/45_puzzle', 'community/50_community' ].each {
                    def gamesdirname = basename(it)
                    def tmpdir = "${env.TMPDIR}/playit_${gamesdirname}"
                    preparePreCmd += "mkdir -p '${tmpdir}'"
                    prepareCmd += shCwd(tmpdir, [
                        "wget -q -O - https://downloads.dotslashplay.it/games-collections/${it}_current.tar.gz | tar -xz",
                        "mv games '${playitPath}/games/${gamesdirname}'",
                    ])
                }
            }

            preparePostCmd += "cp ${playitPath}/libplayit2.sh ."
            preparePostCmd += pipelineParams.playit_game_id.collect {
                "cp ${playitPath}/games/*/play-${it}.sh . || true"
            }
        }

        runShList(
            preparePreCmd +
            shParallel(prepareCmd) +
            preparePostCmd
        )
    }

    def patchPlayitCmd(int mrId, String projectName, int projectId) {
        def patchCmd = []

        def patchStatus = sh(
            script: "wget -q -O - 'https://forge.dotslashplay.it/play.it/${projectName}/-/merge_requests/${mrId}.patch' | patch -p2",
            returnStatus: true
        )

        if (patchStatus != 0) {
            def mrBranchName = sh(
                script: "wget -q -O - 'https://forge.dotslashplay.it/api/v4//projects/${projectId}/merge_requests/${mrId}' | jq -r '.source_branch'",
                returnStdout: true
            ).trim()
            def mrChangedFiles = sh(
                script: "wget -q -O - 'https://forge.dotslashplay.it/api/v4//projects/${projectId}/merge_requests/${mrId}/changes' | jq -r '.changes[] | .new_path'",
                returnStdout: true
            ).trim().tokenize()

            mrChangedFiles.each { changedFile ->
                patchCmd += "wget -q 'https://forge.dotslashplay.it/play.it/${projectName}/-/raw/${mrBranchName}/${changedFile}'"
            }
        }

        return patchCmd
    }

    def patchScripts() {
        def patchCmd = []
        if (pipelineParams.playit) {
            pipelineParams.playit_merge_requests_scripts.each {
                patchCmd += patchPlayitCmd(it, "scripts", 19)
            }

            pipelineParams.playit_merge_requests_gmaes.each {
                patchCmd += patchPlayitCmd(it, "games", 9)
            }

            pipelineParams.playit_sedScript.each { sedFile, sedExpr ->
                patchCmd += "sed -i '${sedFile}' -e '${sedExpr.join("' -e '")}'"
            }
        }

        runShList(patchCmd)
    }

    def download() {
        def downloadCmd = []
        if (pipelineParams.gog) {
            def addonalArgs = []
            if (pipelineParams.gog_game_include.size() > 0) {
                addonalArgs += [ '--include', pipelineParams.gog_game_include.join(',') ]
            }

            if (pipelineParams.gog_game_exclude.size() > 0) {
                addonalArgs += [ '--exclude', pipelineParams.gog_game_exclude.join(',') ]
            }

            withFolderProperties {
                if (env.GOG_GAME_LANGUAGE) {
                    addonalArgs += [ '--language', env.GOG_GAME_LANGUAGE + ",en" ]
                }
            }

//             def downloadSuccess = false
//             (0.4).any {
//                 def downloadStatus = sh(
//                     script: setNiceCmd + """
//                         lgogdownloader \
//                             --download \
//                             --no-subdirectories \
//                             --platform linux,windows \
//                             ${addonalArgs.join(' ')} \
//                             --game "${pipelineParams.gog_game_id.join('|')}"
//                     """,
//                     returnStatus: true
//                 )
//
//                 if (downloadStatus == 0) {
//                     downloadSuccess = true
//                     return true // break
//                 }
//             }
//
//             if (!downloadSuccess) {
//                 error("Unable to download ${env.JOB_BASE_NAME} (gog: ${pipelineParams.gog_game_id.join(', ')})")
//             }
            downloadCmd += [
                "mkdir -p download",
                shCwd("download",
                    """lgogdownloader \
                        --download \
                        --no-subdirectories \
                        --platform ${pipelineParams.gog_platform.join(',')} \
                        ${addonalArgs.join(' ')} \
                        --game '^${pipelineParams.gog_game_id.join('$|^')}\$'
                """),
                'mv download/*/* .'
            ]
        }

        if (pipelineParams.playit) {
            pipelineParams.playit_game_id.each {
                downloadCmd += [ "resources", "games" ].collect { urlDir ->
                    """
                        wget \
                            --quiet \
                            --execute robots=off \
                            --recursive \
                            --no-parent \
                            --no-host-directories --cut-dirs=2 \
                            --reject "index.html*" \
                            https://downloads.dotslashplay.it/${urlDir}/${it}/ || true
                    """
                }
            }
        }

        downloadCmd += pipelineParams.downloadExtraUrl.collect { filename, url  ->
            "wget -q '${url}' -O '${filename}'"
        }

        runShList(downloadCmd)
    }

    def pack() {
        def packCmd = []
        if (pipelineParams.playit) {
            def addonalArgs = []
            if (pipelineParams.playit_current) {
                addonalArgs += [ '--tmpdir', env.TMPDIR ]
            }
            withFolderProperties {
                if (compressionMethod) {
                    addonalArgs += [ '--compression', compressionMethod ]
                }

                if (env.INSTALL_PREFIX) {
                    addonalArgs += [ '--prefix', env.INSTALL_PREFIX ]
                }

                if (env.PKG_ARCH) {
                    addonalArgs += [ '--architecture', env.PKG_ARCH ]
                }
            }

            packCmd += 'export PLAYIT_LIB2="$PWD/libplayit2.sh"'
            packCmd += pipelineParams.playit_game_id.collect {
                "./play-${it}.sh ${addonalArgs.join(' ')}"
            }
        }

        runShList(packCmd)
    }

    def signPackages() {}
    def packagesWithNewVersion() {}

    def checkGameUpdated() {
        def filesToDownload = []
        boolean fileHandled = true

        if (pipelineParams.gog) {
            def addonalArgs = []
            if (pipelineParams.gog_game_include.size() > 0) {
                addonalArgs += [ '--include', pipelineParams.gog_game_include.join(',') ]
            }

            if (pipelineParams.gog_game_exclude.size() > 0) {
                addonalArgs += [ '--exclude', pipelineParams.gog_game_exclude.join(',') ]
            }

            withFolderProperties {
                if (env.GOG_GAME_LANGUAGE) {
                    addonalArgs += [ '--language', env.GOG_GAME_LANGUAGE + ",en" ]
                }
            }

            filesToDownload = sh(
                script: """
                    lgogdownloader \\
                        --list details \\
                        --no-subdirectories \\
                        --platform ${pipelineParams.gog_platform.join(',')} \\
                        ${addonalArgs.join(' ')} \\
                        --game '^${pipelineParams.gog_game_id.join('$|^')}\$' 2> /dev/null | \\
                            awk '\$1 == "path:" { print \$NF }' | \\
                            rev | cut -d / -f 1 | rev
                """,
                returnStdout: true
            ).trim().tokenize()
        }

        if (pipelineParams.playit) {
            filesToDownload.any {
                withEnv([ "FILE_NAME=${it}" ]) {
                    def filesInScript = sh(
                        script: '''grep -q --regexp="ARCHIVE_.\\\\+=['\\"]${FILE_NAME}['\\"]" *.sh''',
                        returnStatus: true
                    )

                    if (filesInScript != 0) {
                        fileHandled = false
                        return true // break
                    }
                }
            }
        }

        if (!fileHandled) {
            error('One of files are not handled by scripts')
        }
    }

    def artifacts() {}
}
