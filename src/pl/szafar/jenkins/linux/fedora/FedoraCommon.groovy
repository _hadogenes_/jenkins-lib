package pl.szafar.jenkins.linux.fedora

import pl.szafar.jenkins.linux.LinuxCommon

class FedoraCommon extends LinuxCommon {
    FedoraCommon(def jenkins) {
        super(jenkins)
    }

    def systemUpdate(boolean isAgentRunningDocker) {
        sh '''
            [ "$NICE_LEVEL" ] && renice -n $NICE_LEVEL $$
            sudo dnf update --refresh -y --enablerepo='*source'
            dnf info test 2> /dev/null || true
        '''
    }

    def addCustomRepo() {
//         withFolderProperties {
//             if (env.REPO_URL) {
//                 sh '''
//                     (
//                         echo "[$REPO_NAME]"
//                         echo "SigLevel = Required"
//                         echo "Server = $REPO_URL"
//                     ) | sudo tee -a /etc/pacman.conf
// 
//                     sudo pacman -Sy
//                 '''
//             }
//         }
    }

    def importGpgKeyToSystem() {
        withFolderProperties {
            sh '''
                rm -f public-key.gpg
                gpg --batch --armor --output public-key.gpg --export "$GPGKEY" > /dev/null 2>&1
                sudo rpm --import public-key.gpg > /dev/null 2>&1

                PACKAGER=$(gpg --import --import-options show-only --with-colons public-key.gpg | awk -F ':' '$1 == "uid" { print $10 }')

                (
                    echo %_signature gpg
                    echo %_gpg_path $HOME/.gnupg
                    echo %_gpg_name $PACKAGER
                    echo %_gpgbin /usr/bin/gpg
                ) >> "$HOME/.rpmmacros"

                (
                    echo use-agent
                    echo pinentry-mode loopback
                ) >> "$HOME/.gnupg/gpg.conf"

                (
                    echo allow-loopback-pinentry
                ) >> "$HOME/.gnupg/gpg-agent.conf"
            '''
        }
    }
    def cleanup() {
        dir("cleanup") {
            sh '''
                sudo find /etc/yum.repos.d -type f -exec sed '/^exclude=/d' -i {} +
                dnf list --installed | awk '$3 !~ /^(@fedora|@updates)/ {print $1}' | tail -n +2 | xargs --no-run-if-empty sudo dnf downgrade -y -q --allowerasing --disablerepo=\\* --enablerepo=fedora,updates > /dev/null || true
                dnf copr list | cut -d ' ' -f 1 | xargs --no-run-if-empty -n 1 sudo dnf copr remove -q > /dev/null

                dnf repoquery --installed | xargs sudo dnf mark remove > /dev/null
                requiredPkgs="$(grep -vh '^#' /etc/dnf/protected.d/*) java-headless git openssh-server docker parallel gawk rpmdevtools rpm-sign wget libuv dnf-command(copr) dnf-command(builddep) dnf-command(download) dnf"
                sudo dnf install -y -q $requiredPkgs > /dev/null || true
                sudo dnf mark install $requiredPkgs > /dev/null || true
                sudo dnf autoremove -y -q > /dev/null
                sudo dnf clean packages -y > /dev/null
            '''
        }
        super.cleanup()
    }
}
