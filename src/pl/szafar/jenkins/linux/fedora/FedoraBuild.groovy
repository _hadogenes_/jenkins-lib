package pl.szafar.jenkins.linux.fedora

import javax.xml.xpath.*
import javax.xml.parsers.DocumentBuilderFactory
import pl.szafar.jenkins.linux.LinuxBuild

class FedoraBuild extends LinuxBuild {
    def pkgList = [:]
    def artifactsSend = false
    def fedoraRelease = null
    def preBuildCmd = []
    def postBuildCmd = []

    FedoraBuild(def jenkins) {
        super(jenkins)
        common = new FedoraCommon(jenkins)
    }

    def init(body) {
        pipelineParams << [
            ignorePackagesDeps: true,
            withThirdpartyDeps: true,
            preDownloadCmds: [],
            groupPackages: [],
            customRepoRpm: [],
            srpm: false,
            coprRepo: null,
            gitBase: null,
            versionFromGitLatestTag: false,
            versionFromCmd: null,
            versionFromXPath: null,
            withCond: [],
            withBCond: [],
            witouthCond: [],
            witouthBCond: [],
            srpmToRpmRegex: null,
            downloadSrpmInPreBuild: false,
            specFromTemplate: false,
            sedSpecUseEnv: false,
            sedSpec: []
        ]

        triggerCron = 'H 3 * * *'
        triggerUrlCron = 'H */2 * * *'

        def argPipelineParams = super.init(body)

        preBuildAgentLabel << "(fedora||docker)"
        buildAgentLabel << "(fedora||docker)"

        preBuildCmd += [
            'cp -a /usr/lib/rpm/macros.d "$TMPDIR/rpm_macros.d"',
            'cp -a /etc/yum.repos.d "$TMPDIR/yum.repos.d"',
            '''
                sudo find /usr/lib/rpm/macros.d -type f -exec sed \
                    -e '/__ninja_common_opts/s/ -v//' \
                    -e 's/-DCMAKE_VERBOSE_MAKEFILE[^" ]*/ /' \
                    -e 's/ --verbose//' \
                    -i {} +
            '''
        ]

        postBuildCmd += [
            '[ ! -d "$TMPDIR/rpm_macros.d" ] || sudo find "$TMPDIR/rpm_macros.d" -type f -exec mv {} /usr/lib/rpm/macros.d/ \\;',
            '[ ! -d "$TMPDIR/yum.repos.d" ] || sudo find "$TMPDIR/rpm_macros.d" -type f -exec mv {} /etc/yum.repos.d \\;',
        ]

        withFolderProperties {
            if (env.CUSTOM_REPO_RPM) {
                pipelineParams.customRepoRpm += env.CUSTOM_REPO_RPM.tokenize(';')
                pipelineParams.customRepoRpm.unique()
            }

            if (env.CUSTOM_REPO_COPR) {
                if (pipelineParams.coprRepo == null) {
                    pipelineParams.coprRepo = env.CUSTOM_REPO_COPR
                }
            }

            if (!argPipelineParams.containsKey('srpm')) {
                if (env.SRPM) {
                    pipelineParams.srpm = env.SRPM.toBoolean()
                }
                else if ((pipelineParams.coprRepo != null) || (pipelineParams.customRepoRpm.size() > 0)) {
                    pipelineParams.srpm = true
                }
            }

            if (pipelineParams.specFromTemplate) {
                if (!env.GIT_SPECS_URL) {
                    error("env GIT_SPECS_URL not set")
                }
            }

            if (pipelineParams.versionFromGitLatestTag) {
                if (pipelineParams.gitBase == null) {
                    error("gitBase must be set when versionFromGitLatestTag is true")
                }
            }
        }

        if (!argPipelineParams.containsKey('ignoreJobName')) {
            if (pipelineParams.groupPackages.size() > 0) {
                pipelineParams.ignoreJobName = true
            }
        }

        if (pipelineParams.srpmToRpmRegex instanceof String) {
            pipelineParams.srpmToRpmRegex = [ pipelineParams.srpmToRpmRegex, "" ]
        }

//         if (pipelineParams.coprRepo != null) {
//             if (!argPipelineParams.containsKey('gitBase')) {
//                 pipelineParams.gitBase = "http://copr-dist-git.fedorainfracloud.org/git/${pipelineParams.coprRepo}"
//             }
//         }

        pipelineParams.withBCond.each {
            pipelineParams.sedSpec += [
                "s/bcond_with\\s*${it}\\( \\|\$\\)/bcond_without ${it} /"
            ]
        }
        pipelineParams.withCond.each {
            [ 'define', 'global' ].each { define ->
                pipelineParams.sedSpec += [
                    "s/%${define}\\s*${it}\\s.*/%${define} ${it} 1/",
                    "s/%${define}\\s*with_${it}\\s.*/%${define} with_${it} 1/",
                    "s/%${define}\\s*without_${it}\\s.*/%${define} without_${it} 0/"
                ]
            }
        }

        pipelineParams.withoutBCond.each {
            pipelineParams.sedSpec += [
                "s/bcond_without\\s*${it}\\( \\|\$\\)/bcond_with ${it} /"
            ]
        }

        pipelineParams.withoutCond.each {
            [ 'define', 'global' ].each { define ->
                pipelineParams.sedSpec += [
                    "s/%${define}\\s*${it}\\s.*/%${define} ${it} 0/",
                    "s/%${define}\\s*with_${it}\\s.*/%${define} with_${it} 0/",
                    "s/%${define}\\s*without_${it}\\s.*/%${define} without_${it} 1/"
                ]
            }
        }
        
//         if (!argPipelineParams.containsKey('downloadSrpmInPreBuild')) {
//             if (pipelineParams.sedSpec.size() > 0) {
//                 pipelineParams.downloadSrpmInPreBuild = true
//             }
//         }

        if (currentBuild.previousBuild != null) {
            if (currentBuild.previousBuild.resultIsWorseOrEqualTo("FAILURE")) {
                FedoraBuildStep failCause = currentBuild.previousBuild.buildVariables.BUILD_STEP
                if (failCause == FedoraBuildStep.ARTIFACTS) {
                    if (currentBuild.previousBuild.buildVariables.BUILD_NODE) {
                        buildAgentLabel << currentBuild.previousBuild.buildVariables.BUILD_NODE
                    }
                }
            }
        }

        if (currentBuild.previousSuccessfulBuild != null) {
            def prevFedoraRelease = null
            def prevPackages = null

            try {
                prevFedoraRelease = currentBuild.previousBuild.buildVariables.FEDORA_RELEASE
                prevPackages = currentBuild.previousBuild.buildVariables.PACKAGE_TO_BUILD.split(',')
            }
            catch (Exception e) {
                prevFedoraRelease = null
                prevPackages = null
            }

            if (prevFedoraRelease && prevPackages) {
                withFolderProperties {
                    if (env.PACKAGES_TYPE == "system") {
                        prevPackages.each {
                            urlTriggerEntries << URLTriggerEntry(
                                url: "https://src.fedoraproject.org/_dg/bodhi_updates/rpms/${it}",
                                timeout: 200,
                                requestHeaders: [
                                    RequestHeader( headerName: "Accept", headerValue: "application/json" )
                                ],
                                contentTypes: [
                                    JsonContent([ JsonContentEntry( jsonPath: "\$.updates.F${prevFedoraRelease}.stable" ) ])
                                ]
                            )
                        }
                    }
                    else if (env.PACKAGES_TYPE == "rpmfusion") {
                        prevPackages.each {
                            [ 'releases', 'updates' ].each { repoType ->
                                urlTriggerEntries << URLTriggerEntry(
                                    url: "https://download1.rpmfusion.org/free/fedora/${repoType}/${prevFedoraRelease}/Everything/source/SRPMS/repoview/${it}.html",
                                    timeout: 200,
                                    requestHeaders: [
                                        RequestHeader( headerName: "Accept" , headerValue: "application/xml" )
                                    ],
                                    contentTypes: [
                                        XMLContent([ XMLContentEntry( xPath: '//a[@class="inpage"]' ) ])
                                    ]
                                )
                            }
                        }
                    }

                    if (pipelineParams.gitBase != null) {
                        if (pipelineParams.gitBase.indexOf('github.com') != -1) {
                            def githubProject = pipelineParams.gitBase.replaceAll(/.*github.com\//, "")

                            prevPackages.each {
                                def githubUrl = "https://api.github.com/repos/${githubProject}/${it}/commits/master"
                                def githubJsonPath = "\$.sha"
                                if (pipelineParams.versionFromGitLatestTag)
                                {
                                    githubUrl = "https://api.github.com/repos/${githubProject}/${it}/tags"
                                    githubJsonPath = "\$[0].name"
                                }

                                urlTriggerEntries << URLTriggerEntry(
                                    url: githubUrl,
                                    timeout: 200,
                                    requestHeaders: [
                                        RequestHeader( headerName: "Accept", headerValue: "application/json" )
                                    ],
                                    contentTypes: [
                                        JsonContent([ JsonContentEntry( jsonPath: githubJsonPath ) ])
                                    ]
                                )
                            }
                        }
                        else if (pipelineParams.gitBase.indexOf('gitlab.com') != -1) {
                            def gitlabProject = pipelineParams.gitBase.replaceAll(/.*gitlab.com\//, "")
                            prevPackages.each {
                                def gitlabProjectId = java.net.URLEncoder.encode("${gitlabProject}/${it}", "UTF-8")

                                def gitlabUrl = "https://gitlab.com/api/v4/projects/${gitlabProjectId}/repository/commits"
                                def gitlabJsonPath = "\$.id"
                                if (pipelineParams.versionFromGitLatestTag)
                                {
                                    gitlabUrl = "https://gitlab.com/api/v4/projects/${gitlabProjectId}/repository/tags"
                                    gitlabJsonPath = "\$[0].name"
                                }

                                urlTriggerEntries << URLTriggerEntry(
                                    url: gitlabUrl,
                                    timeout: 200,
                                    requestHeaders: [
                                        RequestHeader( headerName: "Accept", headerValue: "application/json" )
                                    ],
                                    contentTypes: [
                                        JsonContent([ JsonContentEntry( jsonPath: gitlabJsonPath ) ])
                                    ]
                                )
                            }
                        }
                    }
                }
            }
        }

        if (pipelineParams.versionFromXPath != null) {
            urlTriggerEntries << URLTriggerEntry(
                url: pipelineParams.versionFromXPath.url,
                timeout: 200,
                requestHeaders: [
                    RequestHeader( headerName: "Accept" , headerValue: "application/xml" )
                ],
                contentTypes: [
                    XMLContent([ XMLContentEntry( xPath: pipelineParams.versionFromXPath.xpath ) ])
                ]
            )
        }
    }

    def getOrCreatePackage(def name, def srpm, def rpm, def version, def gitTag = null) {
        if (name in pkgList) {
            return pkgList[name]
        }

        def pkg = [
            name: name,
            version: version,
            gitTag: gitTag,
            base: null,
            spec: null,
            rpm: rpm,
            srpm: srpm,
            preBuildCmd: [],
            postBuildCmd: []
        ]

        pkg.base = pkg
        pkgList[name] = pkg

        return pkg
    }

    def updatePackageInfo() {
        def pkgToUpdate = packagesToBuild.findAll { !(it in pkgList) }

        if (pkgToUpdate.size() > 0) {
            pkgToUpdate.unique()
            def sourcePackageList = sh(
                script: """
                    dnf info ${pkgToUpdate.join(' ')} |\
                        awk '\$1 == "Name" { name=\$3 } \$1 == "Version" { version=\$3 } \$1 == "Source" && \$3 != "None" { print name"="version";"\$3 }'
                """,
                returnStdout: true
            ).trim().tokenize()

            sourcePackageList.each {
                def keyValue = it.split('=', 2)
                def valueList = keyValue[1].split(';')
                def packageName = keyValue[0]
                def version = valueList[0]
                def srpmName = valueList[1]
                def rpmName = srpmName.replaceAll(/fc[0-9\.]+\.src\.rpm$/, "")
                def srpmToRpmRegex = pipelineParams.srpmToRpmRegex
                if (pipelineParams.srpmToRpmRegex instanceof Map) {
                    srpmToRpmRegex = null
                    if (packageName in pipelineParams.srpmToRpmRegex) {
                        srpmToRpmRegex = pipelineParams.srpmToRpmRegex[packageName]
                    }
                }

                if (srpmToRpmRegex != null) {
                    rpmName = rpmName.replaceAll(srpmToRpmRegex[0], srpmToRpmRegex[1])
                }

                def pkg = getOrCreatePackage(packageName, srpmName, rpmName, version)

                def basePackageName = srpmName[0..srpmName.indexOf("-${version}") - 1]
                if (basePackageName != packageName) {
                    def pkgBase = getOrCreatePackage(basePackageName, srpmName, rpmName, version)
                    pkg.base = pkgBase
                }
            }
        }
    }

    def removePackagesWithSameSrpm(def packagesList) {
        def newPackages = []
        packagesList.each {
            if (it in pkgList) {
                newPackages << pkgList[it].base.name
            }
            else {
                newPackages << it
            }
        }

        return newPackages.unique(false)
    }

    def setVersionForAllPkgs(def version) {
        packagesToBuild.each {
            def pkg = pkgList[it]
            if (pkg.version == null) {
                pkg.version = version
            }
        }

        if (pipelineParams.specFromTemplate) {
            pipelineParams.sedSpec << "s/@VERSION@/${version}/"
        }
    }

    def getThirdpartyDepsOfPackage(def pkgNames) {
        def thirdpartyDeps = []
        if (pkgNames.size() > 0) {
            thirdpartyDeps += sh(
                script: """
                    sudo dnf install --assumeno --skip-broken ${pkgNames} 2> /dev/null |\
                        awk '(\$0 ~ /^ /) && (\$4 !~ /^(fedora|updates)/) && (\$1 != "Package") { print \$1 }'
                """,
                returnStdout: true
            ).trim().tokenize()
        }

        return thirdpartyDeps
    }
    
    def findWildcardPackages(def packageList) {
        def packagesWildcard = []
        def packagesFromRegex = []

        packageList.each {
            if (it.indexOf('*') != -1) {
                def pkgRegex = it.replaceAll('\\*', '.*')
                pkgList.keySet().each { pkgname ->
                    if (pkgname ==~ pkgRegex) {
                        packagesFromRegex << pkgname
                    }
                }
                packagesWildcard << it
            }
        }

        return [ packagesWildcard, packagesFromRegex ]
    }

    def prepare(boolean preBuild = false) {
        if (!preBuild) {
            env.BUILD_NODE = env.NODE_NAME
        }
        env.BUILD_STEP = FedoraBuildStep.PREPARE.toString()
        if (pipelineParams.customRepoRpm.size() > 0) {
            sh "sudo dnf install -y ${pipelineParams.customRepoRpm.join(' ')}"
        }

        if (pipelineParams.coprRepo != null) {
            sh "sudo dnf copr enable -y ${pipelineParams.coprRepo}"
        }

        sh 'rm -f $HOME/.rpmmacros $HOME/.rpmrc'

        super.prepare(preBuild)

        fedoraRelease = sh(script: 'rpm -E %fedora', returnStdout: true).trim()

        def firstRun = preBuild || pipelineParams.forceBuild

        if (firstRun) {
            if (pipelineParams.srpm) {
                if (pipelineParams.groupPackages.size() > 0) {
                    def groupPackages = sh(
                        script: """
                            dnf group info '${pipelineParams.groupPackages.join("' '")}' | awk '\$0 ~ /^  / {print \$1}'
                        """,
                        returnStdout: true
                    ).trim().tokenize()

                    packagesToBuild += groupPackages
                }

                if (pipelineParams.withThirdpartyDeps) {
                    packagesToBuild += getThirdpartyDepsOfPackage(packagesToBuild.join(' '))
                    if (pipelineParams.ignorePackagesDeps) {
                        pipelineParams.ignorePackages += getThirdpartyDepsOfPackage(pipelineParams.ignorePackages.join(' '))
                    }
                }

                def packagesWildcard
                def packagesFromRegex
                def ignoreWildcard
                def ignoreFromRegex
                updatePackageInfo()
                (packagesWildcard, packagesFromRegex) = findWildcardPackages(packagesToBuild)
                (ignoreWildcard, ignoreFromRegex) = findWildcardPackages(pipelineParams.ignorePackages)

                packagesToBuild += packagesFromRegex
                pipelineParams.ignorePackages += packagesWildcard
                pipelineParams.ignorePackages += ignoreFromRegex
                
                packagesToBuild = removePackagesWithSameSrpm(packagesToBuild)
                pipelineParams.ignorePackages = removePackagesWithSameSrpm(pipelineParams.ignorePackages)
            }
            else {
                packagesToBuild.each {
                    getOrCreatePackage(it, null, null, null)
                }
            }

            packagesToBuild -= pipelineParams.ignorePackages

            env.FEDORA_RELEASE = fedoraRelease
            env.PACKAGE_TO_BUILD = packagesToBuild.join(',')

            if (pipelineParams.versionFromGitLatestTag) {
                packagesToBuild.each {
                    def pkg = pkgList[it]
                    def gitTags = sh(
                        script: "git ls-remote --tags --refs '${pipelineParams.gitBase}/${pkg.name}.git' | cut -d '/' -f 3- | sort -V",
                        returnStdout: true
                    ).trim().tokenize()

                    gitTags.reverse().any { gitTag ->
                        def newversion = gitTag.replaceAll(/^[^0-9]*/, "")
                        if ((!newversion) || (gitTag.indexOf('alpha') != -1) || (gitTag.indexOf('beta') != -1) || (gitTag.indexOf('nightly') != -1)) {
                            return false // continue
                        }

                        pkg.version = newversion
                        pkg.gitTag = gitTag

                        if (pipelineParams.specFromTemplate) {
                            pipelineParams.sedSpec << "s/@VERSION@/${newversion}/"
                        }

                        return true // break
                    }
                }
            }

            if (pipelineParams.versionFromXPath != null) {
                def xpath = XPathFactory.newInstance().newXPath()
                def builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()

                def inputStream = new URL(pipelineParams.versionFromXPath.url).openConnection().getInputStream();
                def records = builder.parse(inputStream).documentElement
                def version = xpath.evaluate( pipelineParams.versionFromXPath.xpath, records ).trim()

                pipelineParams.versionFromXPath.replace.each {
                    version = version.replaceAll(it[0], it[1])
                }
                if (version == "") {
                    error("Cannot find version from XPath: ${pipelineParams.versionFromXPath.xpath}")
                }

                setVersionForAllPkgs(version)
            }

            if (pipelineParams.versionFromCmd != null) {
                def version = ""
                dir(env.TMPDIR) {
                    version = sh(
                        script: pipelineParams.versionFromCmd,
                        returnStdout: true
                    ).trim()
                }

                if (version == "") {
                    error("Cannot find version from cmd: ${pipelineParams.versionFromCmd}")
                }

                setVersionForAllPkgs(version)
            }
        }

        def pkgNotFound = packagesToBuild.findAll { !(it in pkgList) }
        if (pkgNotFound.size() > 0) {
            error("Packages not found in database: ${pkgNotFound.join(', ')}")
        }
//         if (!preBuild && !(pipelineParams.forceBuild && pipelineParams.aur)) {
//             common.addCustomRepo()
//         }
    }

    def download(boolean preBuild = false) {
        env.BUILD_STEP = FedoraBuildStep.DOWNLOAD.toString()
        super.download(preBuild)
        dir(env.BUILDDIR) {
            downloadSpecs(preBuild)
            applyPatches()
            if (!preBuild) {
                //checkAlreadyBuild()
                downloadSources()
                installBasicDeps()
            }
        }
    }

    def preBuild() {
        env.BUILD_STEP = FedoraBuildStep.PREBUILD.toString()
        super.preBuild()
        dir(env.BUILDDIR) {
            packagesWithNewVersion()
        }
    }

    def extract() {
        dir(env.BUILDDIR) {
            extractSources()
            installBuildReqsDeps()
            if (!pipelineParams.repackbuild) {
                applyCompilationFlags()
            }
        }
    }

    def building() {
        dir(env.BUILDDIR) {
            buildSources()
        }
    }

    def downloadSpecs(boolean preBuild = false) {
        def downloadPreCmd = [
            "rm -rf ${packagesToBuild.join(' ')}",
            "mkdir ${packagesToBuild.join(' ')}"
        ]
        def downloadCmd = []
        def downloadSrpmList = []

        if (pipelineParams.specFromTemplate) {
            downloadCmd += packagesToBuild.collect {
                "cp '${env.WORKSPACE}/specs/${it}.spec.in' '${it}/${it}.spec'"
            }
        }
        else if (pipelineParams.gitBase != null) {
            packagesToBuild.each {
                def pkg = pkgList[it]
                def gitCmd = "git clone --single-branch"
                if (pipelineParams.versionFromGitLatestTag) {
                    gitCmd += " --branch ${pkg.gitTag}"
                }
                gitCmd += " '${pipelineParams.gitBase}/${pkg.name}.git' ."

                downloadCmd << "cd ${pkg.name} && ${gitCmd}"
            }
        }

        def downloadSrpm = false

        if (pipelineParams.srpm) {
            downloadSrpm = (pipelineParams.downloadSrpmInPreBuild || !preBuild)
        }

        if (downloadSrpm) {
            def packageToDownload = packagesToBuild.collect {
                def pkg = pkgList[it]
                return "${pkg.name}-${pkg.version.replaceAll(/-[^-]*/, "")}"
            }
            downloadPreCmd << "dnf download --source ${packageToDownload.join(' ')}"
            downloadCmd += packagesToBuild.collect {
                def pkg = pkgList[it]
                "cd ${pkg.name} && rpm2cpio ../${pkg.srpm} | cpio -idm"
            }
        }

        sh setNiceCmd + downloadPreCmd.join('\n') + """
            echo "${downloadCmd.join('\n')}" | parallel
        """

        findFiles().each {
            def specList = findFiles(glob: "${it.name}/*.spec")
            if (specList.size() > 0) {
                if (specList.size() > 1) {
                    error("More than one spec files (${specList.join(', ')})")
                }

                def pkg = pkgList[it.name]
                pkg.spec = specList[0].name
            }
        }
    }

    def downloadSources() {
        if (pipelineParams.preDownloadCmds.size() > 0) {
            sh setNiceCmd + pipelineParams.preDownloadCmds.join("\n")
        }

        def downloadCmd = []
        packagesToBuild.each {
            def pkg = pkgList[it]
            def spectoolArgs = [
                "--define", "_sourcedir ${pwd()}/${pkg.name}",
                "--get-files", "${pkg.spec}"
            ]

            downloadCmd << "cd ${pkg.name} && spectool '${spectoolArgs.join("' '")}'"
        }

        if (pipelineParams.manualDownload) {
            if (isStartedByUser) {
                notify("Waiting ${currentBuild.projectName}", "Now copy the files for ${currentBuild.fullProjectName}")
                input(id: 'userInput', message: 'Now copy the files')
            }
            else {
                error('Manual download requested')
            }
        }

        sh setNiceCmd + """
            echo "${downloadCmd.join('\n')}" | parallel
        """
    }

    def applyPatches() {
        packagesToBuild.each {
            def pkg = pkgList[it]
            if (pkg.spec != null) {
                def patchCmd = []

                def patchDirPath = "${env.WORKSPACE}/patch/${it}"
                if (fileExists(patchDirPath) || pipelineParams.sedSpec.size() > 0) {
                    if (fileExists(patchDirPath)) {
                        patchCmd += [
                            "cp -rv '${patchDirPath}/.' .",
                            "patch -p1 -i spec.patch"
                        ]
                    }

                    if (pipelineParams.sedSpec.size() > 0) {
                        def sedSpec = pipelineParams.sedSpec

                        if (pipelineParams.sedSpecUseEnv) {
                            def engine = new groovy.text.SimpleTemplateEngine()
                            def binding = [env : env]
                            sedSpec = pipelineParams.sedSpec.collect { engine.createTemplate(it).make(binding).toString() }
                        }

                        patchCmd << "sed -i ${pkg.spec} -e '${sedSpec.join("' -e '")}'"
                    }

                    dir(pkg.name) {
                        sh setNiceCmd + patchCmd.join('\n')
                    }
                }
            }
        }
    }

    def installBasicDeps() {
        if (pipelineParams.addonalDepends.size() > 0) {
            sh setNiceCmd + """
                sudo dnf install -y ${pipelineParams.addonalDepends.join(' ')}
            """
        }

        def builddepList = []
        if ((pipelineParams.srpm) && (pipelineParams.sedSpec.size() == 0)) {
            builddepList = packagesToBuild.collect { def pkg = pkgList[it]; pkg.srpm }
        }
        else {
            builddepList = packagesToBuild.collect { def pkg = pkgList[it]; "${pkg.name}/${pkg.spec}" }
        }
        if (builddepList.size() > 0) {
            sh setNiceCmd + """
                sudo dnf builddep -y --allowerasing ${builddepList.join(' ')}
            """
        }
    }

    def installBuildReqsDeps() {
        packagesToBuild.each {
            def pkg = pkgList[it]
            dir(pkg.name) {
                sh setNiceCmd + """
                    rpmbuild \
                        -br \
                        --noprep \
                        --nodeps \
                        --define '_topdir ${pwd()}/build' \
                        --define '_sourcedir ${pwd()}' \
                        ${pkg.spec} || true
                """
            }
        }

        def buildreqsList = findFiles(glob: "*/build/SRPMS/*.buildreqs.nosrc.rpm").collect { it.path }

        if (buildreqsList.size() > 0) {
            sh setNiceCmd + """
                sudo dnf builddep -y ${buildreqsList.join(' ')}
            """
        }
    }

    def extractSources() {
        env.BUILD_STEP = FedoraBuildStep.EXTRACT.toString()
        packagesToBuild.each {
            def pkg = pkgList[it]
            dir(pkg.name) {
                sh setNiceCmd + """
                    rpmbuild \
                        -bp \
                        --define '_topdir ${pwd()}/build' \
                        --define '_sourcedir ${pwd()}' \
                        ${pkg.spec}
                """
            }
        }
    }

    def applyCompilationFlags() {
        if (fileExists("${env.WORKSPACE}/compilation-flags")) {
            sh """
                cat ${env.WORKSPACE}/compilation-flags/rpmmacros >> "${env.HOME}/.rpmmacros"
                cat ${env.WORKSPACE}/compilation-flags/rpmrc >> "${env.HOME}/.rpmrc"
            """

            sh 'ls $WORKSPACE/compilation-flags/sets/* | xargs sed "s/#.*//" > "$TMPDIR/compilationSets"'
            packagesToBuild.each {
                def pkg = pkgList[it]
                def specialCflagsSets = sh(
                    script: """
                        awk 'match("${pkg.name}", \$1) { for (i = 2; i <= NF; ++i) print \$i; exit }' "${env.TMPDIR}/compilationSets"
                    """,
                    returnStdout: true
                ).trim().tokenize()

                if (specialCflagsSets.size() > 0) {
                    pkg.preBuildCmd << "cp ${env.HOME}/.rpmmacros{,.bak}"
                    pkg.postBuildCmd << "mv ${env.HOME}/.rpmmacros{.bak,}"

                    specialCflagsSets.each { cflagsSet ->
                        if (cflagsSet.startsWith('no-')) {
                            def flagName = cflagsSet.substring("no-".length()).replace('-', '_')
                            pkg.preBuildCmd << "echo '%__${flagName}_cflags %{nil}' >> ${env.HOME}/.rpmmacros"
                        }
                        else if (cflagsSet.startsWith('only-')) {
                            def flagName = cflagsSet.substring("only-".length()).replace('-', '_')
                            pkg.preBuildCmd << "echo '%__opt_cflags %{__${flagName}_cflags}' >> ${env.HOME}/.rpmmacros"
                        }
                    }
                }
            }
        }
    }

    def buildSources() {
        sh setNiceCmd + preBuildCmd.join('\n')
        packagesToBuild.each {
            def pkg = pkgList[it]
            dir(pkg.name) {
                env.BUILD_STEP = FedoraBuildStep.BUILD.toString()
                def buildCmd = []
                buildCmd += pkg.preBuildCmd
                buildCmd << """
                    rpmbuild \
                        -bb \
                        --noprep \
                        --nocheck \
                        --define '_topdir ${pwd()}/build' \
                        --define '_sourcedir ${pwd()}' \
                        --define '_rpmdir ${env.PKGDEST}' \
                        ${pkg.spec}
                """
                buildCmd += pkg.postBuildCmd

                sh setNiceCmd + buildCmd.join('\n')
            }

            signPackages()
            artifacts()
        }
        env.BUILD_STEP = FedoraBuildStep.CLEANUP.toString()
    }

    def signPackages() {
        dir(env.PKGDEST) {
            def signRpms = findFiles(glob: "*/*.rpm").collect { it.path }
            sh "rpm --addsign ${signRpms.join(' ')}"
        }
    }

    def artifacts() {
        env.BUILD_STEP = FedoraBuildStep.ARTIFACTS.toString()
        dir(env.PKGDEST) {
            archiveArtifacts(artifacts: '*/*.rpm', fingerprint: true, onlyIfSuccessful: true)
            deleteDir()
        }
        artifactsSend = true
    }

    def checkAlreadyBuild() {
        dir(env.PKGDEST) {
            sh '''
                mkdir -p old
                rm -rf ../old
                mv old ..
                find -type d -exec mkdir -p ../old/{} \\;
                find -type f -exec mv {} ../old/{} \\;
                mv ../old .
                cd old
                find -type f > ../file.list
                find -type d -exec mkdir -p ../{} \\;
            '''
            def newPackages = []
            packagesToBuild.each {
                def pkg = pkgList[it]
                def packageFiles = sh(
                    script: "rpmspec -q ${env.BUILDDIR}/${pkg.name}/${pkg.spec} 2> /dev/null",
                    returnStdout: true
                ).trim().tokenize().collect { "${it.replaceAll(/.src$/, '')}.rpm" }

                def updatePackage = sh(
                    script: "set +x; for package in ${packageFiles.join(' ')}; do grep -q \$package file.list || exit 1; done",
                    returnStatus: true
                )

                if (updatePackage == 0) {
                    echo "Found ${it} artifacts, skipping building it"
                    dir('old') {
                        sh "set +x; find \\( -name ${packageFiles.join(' -or -name ')} \\) -exec mv {} ../{} \\;"
                    }
                }
                else {
                    newPackages << it
                }
            }

            packagesToBuild = newPackages
            sh '''
                rm -rf old
                find -type d -empty -delete
            '''
        }
    }

    def packagesWithNewVersion() {
        def newPackages = []
        def repoListName
        withFolderProperties {
            copyArtifacts(projectName: env.DEPLOY_JOB)
            repoListName = "${fedoraRelease}-${env.REPO_NAME}.list"
        }

        packagesToBuild.each {
            def pkg = pkgList[it]
            if (pkg.spec != null) {
                def packageFiles = sh(
                    script: "rpmspec -q ${pkg.name}/${pkg.spec} 2> /dev/null",
                    returnStdout: true
                ).trim().tokenize().collect { "${it.replaceAll(/.src$/, '')}" }

                pkg.rpm = packageFiles[0]
            }

            def updatePackage = sh(
                script: "grep -q '${pkg.rpm}' '${repoListName}'",
                returnStatus: true
            )

            if (updatePackage != 0) {
                newPackages << it
            }
        }

        packagesToBuild = newPackages
        pipelineParams.groupPackages.clear()
        pipelineParams.withDeps = false
        pipelineParams.withThirdpartyDeps = false
    }

    def post_step() {
        if (artifactsSend) {
            withFolderProperties {
                build(
                    job: env.DEPLOY_JOB,
                    parameters: [
                        string(name: 'sourceJob', value: env.JOB_NAME),
                        string(name: 'sourceJobNumber', value: env.BUILD_NUMBER),
                        string(name: 'repoName', value: env.REPO_NAME),
                        string(name: 'fedoraRelease', value: fedoraRelease),
                        booleanParam(name: 'forceAdd', value: pipelineParams.forceBuild)
                    ],
                    propagate: false,
                    wait: false
                )
            }
        }
    }

    def cleanup_step() {
        sh setNiceCmd + postBuildCmd.join('\n')
        super.cleanup_step()
    }
}
