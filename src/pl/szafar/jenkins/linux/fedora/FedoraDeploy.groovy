package pl.szafar.jenkins.linux.fedora

import pl.szafar.jenkins.linux.LinuxDeploy

class FedoraDeploy extends LinuxDeploy {
    FedoraDeploy(jenkins) {
        super(jenkins)
        common = new FedoraCommon(jenkins)
    }

    def init(body) {
        super.init(body)

        repoPath = "/repo/${params.fedoraRelease}"
        agentLabel << 'fedorarepo'
    }

    def deploy() {
        def pkgList = [:]
        findFiles(glob: '*/*.rpm').each {
            def repoName = params.repoName
            if (it.name.contains('-debug')) {
                repoName = "debug"
            }
            
            if (!pkgList.containsKey(repoName)) {
                pkgList[repoName] = []
            }
            
            pkgList[repoName] << it.path
        }

        if (!params.forceAdd) {
            def tmpPkgList = [:]
            pkgList.each {
                tmpPkgList[it.key] = it.value.findAll { pkg ->
                    def pkgFileName = new File(pkg).getName()
                    !fileExists("${repoPath}/${it.key}/${pkgFileName}")
                }
            }

            pkgList = tmpPkgList
        }

        pkgList.each {
            if (it.value.size() > 0) {
                sh """    
                    mv ${it.value.join(' ')} '${repoPath}/${it.key}'
                    createrepo --update '${repoPath}/${it.key}'
                """
            }
        }
    }

    def listAllPackagesToFile() {
        sh "find ${repoPath}/${params.repoName} -name '*.rpm' -printf '%f\\n' | sort -u > '${params.fedoraRelease}-${params.repoName}.list'"
    }
}
