package pl.szafar.jenkins.linux.fedora;

public enum FedoraBuildStep {
    INIT,
    PREPARE,
    DOWNLOAD,
    PREBUILD,
    EXTRACT,
    BUILD,
    ARTIFACTS,
    CLEANUP
}
