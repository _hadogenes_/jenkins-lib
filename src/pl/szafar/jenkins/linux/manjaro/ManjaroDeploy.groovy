package pl.szafar.jenkins.linux.manjaro

import pl.szafar.jenkins.linux.LinuxDeploy

class ManjaroDeploy extends LinuxDeploy {
    def ManjaroDeploy(jenkins) {
        super(jenkins)
        common = new ManjaroCommon(jenkins)
    }

    def init(body) {
        super.init(body)
        
        repoPath = "/repo/${params.repoName}"
        agentLabel << 'manjarorepo'
    }

    def deploy() {
        def pkgList = findFiles(glob: '*.pkg.tar.*').collect { it.name }
        pkgList = pkgList.findAll { !it.endsWith(".sig") } // Ignore tar.pkg.gz.sig files
        pkgList = pkgList.findAll { fileExists("${it}.sig") } // Package must be signed
        if (!params.forceAdd) {
            pkgList = pkgList.findAll { !fileExists("${repoPath}/${it}") }
        }

        if (pkgList.size() > 0) {
            def sigList = pkgList.collect { "${it}.sig" }
            sh """
                mv ${pkgList.join(' ')} ${sigList.join(' ')} '${repoPath}/'
                cd '${repoPath}'
                repo-add --verify --sign --remove ${params.repoName}.db.tar.gz ${pkgList.join(' ')}
            """
        }
    }

    def listAllPackagesToFile() {
        sh "find ${repoPath}/ -name '*.pkg.tar.*' -printf '%f\\n' | grep -v '.sig\$' > '${params.repoName}.list'"
    }
}
