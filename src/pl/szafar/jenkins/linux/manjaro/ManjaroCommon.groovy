package pl.szafar.jenkins.linux.manjaro

import pl.szafar.jenkins.linux.LinuxCommon

class ManjaroCommon extends LinuxCommon {
    ManjaroCommon(def jenkins) {
        super(jenkins)
    }

    def systemUpdate(boolean isAgentRunningDocker) {
        sh '''
            [ "$NICE_LEVEL" ] && renice -n $NICE_LEVEL $$
            sudo pacman -Syu --noconfirm --needed glibc
        '''
        if (!isAgentRunningDocker) {
            sh '''
                sudo cp /etc/pacman.conf{,.orig}
                sudo cp /etc/makepkg.conf{,.orig}
            '''
        }
    }

    def setupEnv() {
        super.setupEnv()
        env.MAKEFLAGS = "-j${env.THREAD_NUM.toInteger() + 1}"
        sh 'sudo locale-gen $LANG'
    }

    def addCustomRepo() {
        withFolderProperties {
            if (env.REPO_URL) {
                sh '''
                    (
                        echo "[$REPO_NAME]"
                        echo "SigLevel = Required"
                        echo "Server = $REPO_URL"
                    ) | sudo tee -a /etc/pacman.conf

                    sudo pacman -Sy
                '''
            }
        }
    }

    def getTools(boolean preBuild = false) {
        super.getTools(preBuild)

        if (fileExists('tools/pacman/pacman.conf')) {
            sh '''
                sudo sh -c "cat tools/pacman/pacman.conf >> /etc/pacman.conf"
                sudo pacman -Sy
            '''
        }

        if (!preBuild) {
            if (fileExists('tools/pacman/makepkg.conf')) {
                sh 'sudo sh -c "cat tools/pacman/makepkg.conf >> /etc/makepkg.conf"'
            }
        }

        withFolderProperties {
            if (env.GIT_BB_QUERY_TRUST_URL) {
                dir(".config/bb-query_trust") {
                    git credentialsId: env.GIT_CREDENTIALS_ID, url: env.GIT_BB_QUERY_TRUST_URL
                }
            }
        }
    }

    def importGpgKeyToSystem() {
        withFolderProperties {
            sh '''
                gpg --batch --armor --output public-key.gpg --export "$GPGKEY" > /dev/null 2>&1

                PACKAGER=$(gpg --import --import-options show-only --with-colons public-key.gpg | awk -F ':' '$1 == "uid" { print $10 }')
                echo "PACKAGER='$PACKAGER'" | sudo tee -a /etc/makepkg.conf

                sudo pacman-key --init > /dev/null 2>&1
                sudo pacman-key --add public-key.gpg > /dev/null 2>&1
                sudo pacman-key --lsign-key "$GPGKEY" > /dev/null 2>&1
            '''
        }
    }
}
