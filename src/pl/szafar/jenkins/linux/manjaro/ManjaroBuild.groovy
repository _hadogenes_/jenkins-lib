package pl.szafar.jenkins.linux.manjaro

import pl.szafar.jenkins.linux.LinuxBuild

class ManjaroBuild extends LinuxBuild {
    def dirList = []

    ManjaroBuild(def jenkins) {
        super(jenkins)
        common = new ManjaroCommon(jenkins)
    }
    
    def init(body) {
        pipelineParams << [
            aur: ('aur' in currentBuild.fullProjectName.toLowerCase().tokenize('/')),
            manualCheckVersion: false,
            forceUpdatePkgver: false,
            manualAur: [],
            ignorePackagesUpdate: [],
            dontInstall: [],
            dontInstallAll: false,
            ignorePackageHash: false,
            ignorePkgrel: false,
            sedPkgbuild: []
        ]
        
        def argPipelineParams = super.init(body)

        if (!argPipelineParams.containsKey('withDeps')) {
            pipelineParams.withDeps = pipelineParams.aur && !(pipelineParams.repackbuild || pipelineParams.quickbuild)
        }

        if (!argPipelineParams.containsKey('manualCheckVersion')) {
            pipelineParams.manualCheckVersion = pipelineParams.withDeps || currentBuild.projectName.endsWith("-git")
        }

        if (!argPipelineParams.containsKey('dontInstallAll')) {
            pipelineParams.dontInstallAll = !pipelineParams.aur
        }

        if (!argPipelineParams.containsKey('ignorePackagesUpdate')) {
            pipelineParams.ignorePackagesUpdate = pipelineParams.ignorePackages
        }

        preBuildAgentLabel << "(manjaro||docker)"
        buildAgentLabel << "(manjaro||docker)"

        if (pipelineParams.manualCheckVersion) {
            triggerCron = '0 */12 * * *'
        }

        if (!pipelineParams.ignoreJobName) {
            pipelineParams.dontInstall << currentBuild.projectName
        }

        if (pipelineParams.aur) {
            (packagesToBuild + pipelineParams.manualAur).each {
                urlTriggerEntries << URLTriggerEntry(
                    url: "https://aur.archlinux.org/rpc/?v=5&type=info&arg[]=${it}",
                    timeout: 200,
                    requestHeaders: [
                        RequestHeader( headerName: "Accept", headerValue: "application/json" )
                    ],
                    contentTypes: [
                        JsonContent([ JsonContentEntry( jsonPath: '$.results[0].Version' ) ])
                    ]
                )
            }
        } else {
            urlTriggerEntries << URLTriggerEntry(
                url: 'https://forum.manjaro.org/c/announcements/stable-updates.rss',
                timeout: 200,
                requestHeaders: [
                    RequestHeader( headerName: "Accept" , headerValue: "application/xml" )
                ],
                contentTypes: [
                    XMLContent([ XMLContentEntry( xPath: '/rss/channel/item[1]/title' ) ])
                ]
            )

            packagesToBuild.each {
                urlTriggerEntries << URLTriggerEntry(
                    url: "https://discover.manjaro.org/packages/${it}",
                    timeout: 200,
                    contentTypes: [
                        TextContent([ TextContentEntry( regEx: '.*Version.*' ) ] )
                    ]
                )
            }
        }
    }
    def prepare(boolean preBuild = false) {
        super.prepare(preBuild)
         if (!preBuild && !(pipelineParams.forceBuild && pipelineParams.aur)) {
            common.addCustomRepo()
        }
        genBuildScripts()
        
    }

    def download(boolean preBuild = false) {
        super.download(preBuild)
        dir('build') {
            if (preBuild) {
                if (!pipelineParams.forceBuild) {
                    downloadPkgBuilds(preBuild)
                    applyPatches()
                    regenerateSrcInfo()
                }
            }
            else {
                downloadPkgBuilds(preBuild)
                applyPatches()
                downloadSources()
                regenerateSrcInfo()
                installDeps()
            }
        }
    }

    def preBuild() {
        super.preBuild()
        dir('build') {
            if (!pipelineParams.forceBuild) {
                packagesWithNewVersion()
            }

            if (pipelineParams.aur && (packagesToBuild.size() > 0)) {
                aurCommiter()
            }
        }
    }
    def extract() {
        dir('build') {
            extractSources()
            if (!pipelineParams.repackbuild) {
                applyCompilationFlags()
            }
        }
    }
    
    def building() {
        dir('build') {
            buildSources()
        }
    }

    def cleanup_step() {
        super.cleanup_step()
        if (!isAgentRunningDocker()) {
            sh '''
                sudo cp /etc/pacman.conf{.orig,}
                sudo cp /etc/makepkg.conf{.orig,}
                sudo pacman -Sy
                holdPkgList=$(grep HoldPkg /etc/pacman.conf | cut -d = -f 2- | xargs | sed 's/ /|/g')
                pacman -Qmq | egrep -v "$holdPkgList" | sudo xargs --no-run-if-empty pacman -Rs --noconfirm || true
            '''
        }
    }

    def genBuildScripts() {
        def addonalArgs = []
        def genBuildCmd = []
        def sedDownloadExpr = []
        def sedBuildExpr = []

        if (pipelineParams.aur) {
            genBuildCmd << "bauerbill --aur -S ${addonalArgs.join(" ")} AUR/${packagesToBuild.join(" AUR/")}"

            sedDownloadExpr += [
                '/pbget/!d'
            ]

            sedBuildExpr += [
                's/^pacman /&--noconfirm /',
                's/^makepkgx /&--noconfirm --sign --skipinteg --noextract --nocheck --needed /',
                '/makepkg/ s/-irs/-is/'
            ]

            pipelineParams.ignorePackages.each {
                sedDownloadExpr << "s/ ${it}\\( \\|\$\\)/\\1/g"
                sedBuildExpr << "/pushd ${it}\$/,/popd/d"
            }
        } else {
            if (pipelineParams.withDeps) {
                addonalArgs << "--deps"
            }
            if (pipelineParams.ignorePkgrel) {
                addonalArgs << "--ignore-pkgrel"
            }

            genBuildCmd << "python scripts/manjaro_abs.py ${addonalArgs.join(" ")} ${packagesToBuild.join(" ")}"

            sedDownloadExpr += [
                '/makepkg/d'
            ]

            sedBuildExpr += [
                's/^makepkg /&--noconfirm --sign --skipinteg --noextract --nocheck --needed /',
                '/makepkg/ s/-irs/-is/'
            ]

            pipelineParams.ignorePackages.each {
                sedDownloadExpr << "/pushd ${it}\$/,/popd/d"
                sedBuildExpr << "/pushd ${it}\$/,/popd/d"
            }
        }

        genBuildCmd += [
            "sed -i build/download.sh -e '${sedDownloadExpr.join("' -e '")}'",
            "sed -i build/build.sh -e '${sedBuildExpr.join("' -e '")}'",
        ]

        sh setNiceCmd + genBuildCmd.join('\n')
    }

    def downloadPkgBuilds(boolean preBuild = false) {
        if (fileExists("${env.WORKSPACE}/scripts/pre-download")) {
            sh setNiceCmd + '''
                $WORKSPACE/scripts/pre-download
            '''
        }

        sh setNiceCmd + '''
            ./download.sh
        '''

        if (!preBuild) {
            pipelineParams.manualAur.each {
                if (!fileExists("${it}/PKGBUILD")) {
                    sh "git clone 'https://aur.archlinux.org/${it}.git'" // Use shell command insted of jenkins, because we don't want to track changes
                }
            }

            if (pipelineParams.manualDownload) {
                if (isStartedByUser) {
                    common.notify("Waiting ${currentBuild.projectName}", "Now copy the files for ${currentBuild.fullProjectName}")
                    input(id: 'userInput', message: 'Now copy the files')
                }
                else {
                    error('Manual download requested')
                }
            }
        }

        dirList.clear()
        findFiles().each {
            if (fileExists("${it.name}/PKGBUILD")) {
                dirList << it.name
            }
        }

        if (pipelineParams.dontInstallAll) {
            pipelineParams.dontInstall = dirList
        }
    }

    def downloadSources() {
        def downloadCmd = []

        if (pipelineParams.ignorePackageHash) {
            downloadCmd << "makepkg --geninteg >> PKGBUILD"
        }
        else {
            downloadCmd << "makepkg --verifysource --skippgpcheck --nodeps"
        }

        if (pipelineParams.forceUpdatePkgver) {
            downloadCmd << "makepkg --skippgpcheck --nodeps --nobuild --noextract"
        }

        sh setNiceCmd + """
            echo "${dirList.join('\n')}" |\
            parallel "cd {} && ${downloadCmd.join(' && ')}"
        """
    }

    def applyPatches() {
        dirList.each {
            def patchCmd = []
            def pkgbuildPatchPath = "${env.WORKSPACE}/patch/PKGBUILD/${it}.patch"
            def patchDirPath = "${env.WORKSPACE}/patch/${it}"
            if (fileExists(pkgbuildPatchPath) || fileExists(patchDirPath) || pipelineParams.sedPkgbuild.size() > 0) {
                if (fileExists(pkgbuildPatchPath)) {
                    patchCmd << "patch -p1 -i '${pkgbuildPatchPath}'"
                }

                if (fileExists(patchDirPath)) {
                    patchCmd << "cp -rv '${patchDirPath}/.' ."
                }

                if (pipelineParams.sedPkgbuild.size() > 0) {
                    patchCmd << "sed -i PKGBUILD -e '${pipelineParams.sedPkgbuild.join("' -e '")}'"
                }

                patchCmd << "rm -f .SRCINFO"

                dir(it) {
                    sh setNiceCmd + patchCmd.join('\n')
                }
            }
        }
    }

    def regenerateSrcInfo() {
        sh setNiceCmd + """
            echo "${dirList.join('\n')}" |\
            parallel "[ -f {}/.SRCINFO ] || (cd {} && makepkg --printsrcinfo > .SRCINFO)"
        """
    }

    def installDeps() {
        def dependencyList = new LinkedList(pipelineParams.addonalDepends)
        dirList.each {
            dir(it) {
                dependencyList.addAll(
                    sh(
                        script: '''awk '$1 == "depends" || $1 == "makedepends" { print $3 }' .SRCINFO''',
                        returnStdout: true
                    ).trim().tokenize()
                )
            }
        }

        if (dependencyList.size() > 0) {
            def dependencyToInstall = sh(
                script: """
                    # Remove packages with version in name (ex. name>1) - will be installed in later steps (building)
                    pacman -Slq | sort -u > ${env.TMPDIR}/pacman.list
                    echo "${dependencyList.join('\n')}" | sort -u > ${env.TMPDIR}/depends.list
                    comm -12 "${env.TMPDIR}/pacman.list" "${env.TMPDIR}/depends.list"
                """,
                returnStdout: true
            ).trim().tokenize()

            if (pipelineParams.addonalDependsForce.size() > 0) {
                sh "sudo pacman --noconfirm -S ${pipelineParams.addonalDependsForce.join(' ')}"
            }
            if (dependencyToInstall.size() > 0) {
                sh "sudo pacman --noconfirm --needed -S ${dependencyToInstall.join(' ')}"
            }
        }
    }

    def extractSources() {
        if (fileExists("${env.WORKSPACE}/scripts/pre-extract")) {
            withFolderProperties {
                sh setNiceCmd + '''
                    $WORKSPACE/scripts/pre-extract
                '''
            }
        }

        dirList.each {
            dir(it) {
                sh setNiceCmd + '''
                    makepkg --nobuild --skipinteg --nodeps
                '''
            }
        }
    }

    def applyCompilationFlags() {
        if (fileExists("${env.WORKSPACE}/compilation-flags")) {
            if (fileExists("${env.WORKSPACE}/compilation-flags/global")) {
                sh 'sudo sh -c "cat $WORKSPACE/compilation-flags/global >> /etc/makepkg.conf"'
            }

            sh 'ls $WORKSPACE/compilation-flags/sets/* | xargs sed "s/#.*//" > "$TMPDIR/compilationSets"'
            dirList.each {
                def specialCflagsSets = sh(
                    script: """
                        awk '\$1 == "${it}" || "lib32-"\$1 == "${it}" { for (i = 2; i <= NF; ++i) print \$i; exit }' "${env.TMPDIR}/compilationSets"
                    """,
                    returnStdout: true
                ).trim().tokenize()

                if (specialCflagsSets.size() > 0) {
                    def sedExpr = [
                        "/makepkg.* ${it}\\( \\|\$\\)/i sudo cp /etc/makepkg.conf /etc/makepkg.conf.bak",
                        "/makepkg.* ${it}\\( \\|\$\\)/a sudo mv /etc/makepkg.conf.bak /etc/makepkg.conf"
                    ]

                    specialCflagsSets.each { cflagsSet ->
                        sedExpr << "/makepkg.* ${it}\\( \\|\$\\)/i cat ${env.WORKSPACE}/compilation-flags/flags/${cflagsSet} | sudo tee -a /etc/makepkg.conf"
                    }
                    
                    sh "sed -i build.sh -e '${sedExpr.join("' -e '")}'"
                }
            }
        }
    }

    def buildSources() {
        if (fileExists("${env.WORKSPACE}/scripts/pre-build")) {
            sh setNiceCmd + '''
                $WORKSPACE/scripts/pre-build
            '''
        }


        pipelineParams.manualAur.each {
            dir(it) {
                sh setNiceCmd + '''
                    makepkg -is --noextract --noconfirm --skippgpcheck --sign
                '''
            }
        }

        pipelineParams.dontInstall.each {
            withEnv([ "DONT_INSTALL=${it}" ]) {
                sh '''
                    sed -i build.sh \\
                        -e "s/^pacman.* $DONT_INSTALL/#& /" \
                        -e "/makepkg.* $DONT_INSTALL\\( \\|$\\)/ s/-is/-s/"
                '''
            }
        }

        sh setNiceCmd + '''
            ./build.sh
        '''
    }

    def artifacts() {
        dir(env.PKGDEST) {
            archiveArtifacts(artifacts: '*.pkg.tar.*', fingerprint: true, onlyIfSuccessful: true)
        }
    }

    def packagesWithNewVersion() {
        def newPackages = []
        withFolderProperties {
            copyArtifacts(projectName: env.DEPLOY_JOB)

            dirList.each {
                dir(it) {
                    if (it.endsWith('-git') || pipelineParams.forceUpdatePkgver) {
                        // Automatically updates PKGBUILD with new version
                        sh setNiceCmd + '''
                            makepkg --skippgpcheck --nodeps --verifysource > /dev/null 2>&1
                            makepkg --skippgpcheck --nodeps --nobuild > /dev/null 2>&1
                        '''
                    }

                    def packageVer = sh(
                        script: '''
                            awk '
                                $1 == "pkgver" { pkgver=$3 }
                                $1 == "pkgrel" { pkgrel=$3 }
                                $1 == "epoch" { epoch=$3 }
                                END {
                                    version=pkgver"-"pkgrel
                                    if (epoch)
                                        version=epoch":"version
                                    print version
                                }
                            ' .SRCINFO
                        ''',
                        returnStdout: true
                    ).trim()

                    def packages = sh(
                        script: '''awk -F ' = ' '$1 == "pkgname" { print $2 }' .SRCINFO''',
                        returnStdout: true
                    ).trim().tokenize()

                    def packageName = packages[0]
                    packagesToBuild.any {
                        if (it in packages) {
                            packageName = it
                            return true // break
                        }
                    }

                    def packageFile = "${packageName}-${packageVer}" + '-\\(any\\|x86_64\\).pkg.tar'

                    def updatePackage = sh(
                        script: """grep -q -e "^${packageFile}" -e "^lib32-${packageFile}" "../\$REPO_NAME.list" """,
                        returnStatus: true
                    )

                    if (updatePackage != 0) {
                        newPackages << packageName
                    }
                }
            }
        }

        newPackages -= pipelineParams.ignorePackagesUpdate

        packagesToBuild = newPackages
        pipelineParams.withDeps = false
    }


    def aurCommiter() {
        if (isStartedByUser) {
            withFolderProperties {
                dir(env.HOME + "/.config/bb-query_trust") {
                    def bbEntries = ""

                    withEnv([ 'PACKAGE_IGNORE_REGEX=^AUR\\/' + pipelineParams.ignorePackagesUpdate.join('$|^AUR\\/').trim() + '$' ]) {
                        bbEntries = sh(script: '''#!/bin/bash -xe
                                comm -13 \
                                    <(sort -u < "combinations.txt") \
                                    <(
                                        awk \
                                            '$1 ~ /bb-query_trust/ && $4 !~ /'$PACKAGE_IGNORE_REGEX'/ \
                                                { bbEntry = $4" *"; for (i = 6; $i != "||"; ++i) bbEntry = bbEntry" "$i; print bbEntry }' "$WORKSPACE/build/build.sh" | sort -u
                                    )
                            ''',
                            returnStdout: true
                        ).trim()
                    }

                    if (bbEntries) {
                        common.notify("Waiting ${currentBuild.projectName}", "New packages/maintainers for ${currentBuild.fullProjectName}")
                        def userInput = input(
                            id: 'userInput', message: 'New packages/maintainers:\n' + bbEntries
                        )

                        def newPackages = sh(
                            script: "echo '${bbEntries}' | cut -d ' ' -f 1",
                            returnStdout: true
                        ).trim().tokenize()

                        def bbEntriesCmd = []
                        
                        wrap([$class: 'BuildUser']) {
                            bbEntriesCmd += [
                                "git config user.name ${env.BUILD_USER}",
                                "git config user.email ${env.BUILD_USER_EMAIL}"
                            ]
                        }

                        bbEntriesCmd += [
                            "git branch --set-upstream-to=origin/master master",
                            "git pull",
                            """
                                (
                                    grep -v -e ${newPackages.join(' -e ')} combinations.txt
                                    echo
                                    echo '${bbEntries}'
                                    echo
                                ) | sort -u | tail -n +2 > combinations.txt.new
                            """,
                            "mv combinations.txt.new combinations.txt",
                            "git commit --gpg-sign=${env.GPGKEY} -m 'Add ${env.JOB_BASE_NAME} (and dependencies) to bb-query_trust' combinations.txt",
                            "git push"
                        ]


                        common.importGpgKey(false)
                        common.setIgnoreSshHostKeys()
                        sshagent(credentials: [env.GIT_CREDENTIALS_ID]) {
                            sh setNiceCmd + bbEntriesCmd.join('\n')
                        }
                    }
                }
            }
        } else {
            sh 'sed "/bb-query_trust/!d" build.sh | bash'
        }
    }
}
