package pl.szafar.jenkins.linux.manjaro

import pl.szafar.jenkins.linux.LinuxGame

class ManjaroGame extends LinuxGame {
    ManjaroGame(jenkins) {
        super(jenkins)
        common = new ManjaroCommon(jenkins)
    }

    def init(body) {
        super.init(body) 

        preBuildAgentLabel << "(manjaro||docker)"
        buildAgentLabel << "(manjaro||docker)"
    }

    def cleanup_step() {
        super.cleanup_step()
        if (!isAgentRunningDocker()) {
            sh '''
                sudo cp /etc/pacman.conf{.orig,}
                sudo cp /etc/makepkg.conf{.orig,}
                sudo pacman -Sy
            '''
        }
    }

    def prepare(boolean preGame = false) {
        super.prepare(preGame)
        if (!preGame) {
            if (pipelineParams.addonalDepends.size() > 0) {
                sh 'sudo pacman --noconfirm --needed -S ' + pipelineParams.addonalDepends.join(' ')
            }
        }
    }

    def signPackages() {
        common.importGpgKey(false)

        def signCmd = []
        withFolderProperties {
            findFiles(glob: '*.pkg.tar.*').each {
                signCmd << "gpg --detach-sign --use-agent -u ${env.GPGKEY} --no-armor ${it.name}"
            }
        }

        sh setNiceCmd + signCmd.join('\n')
    }

    def packagesWithNewVersion() {
        withFolderProperties {
            copyArtifacts(projectName: env.DEPLOY_JOB)
        }

        if (pipelineParams.playit) {
            def newPackages = []
            pipelineParams.playit_game_id.each {
                withEnv([ "PLAYIT_GAME_ID=${it}" ]) {
                    def packageName = sh(
                        script: '''
                            ARCHIVES_LIST=$(egrep 'ARCHIVE_[A-Z0-9_]*_VERSION' play-${PLAYIT_GAME_ID}.sh | cut -d = -f 1 | rev | cut -d _ -f 2- | rev | tr '\\n' ' ')
                            (
                                sed '/\\. "$PLAYIT_LIB2"/,$d' play-${PLAYIT_GAME_ID}.sh
                                echo
                                echo 'ARCHIVES_LIST=${ARCHIVES_LIST:-'"$ARCHIVES_LIST"'}'
                                echo 'ARCHIVES_TAB=($(echo $ARCHIVES_LIST))'
                                echo 'VERSION=$(eval echo -n \\$${ARCHIVES_TAB[0]}_VERSION)'
                                echo 'echo ${PKG_MAIN_ID:-$GAME_ID}_${VERSION}+${script_version}'
                            ) | bash
                        ''',
                        returnStdout: true
                    ).trim()

                    withFolderProperties {
                        withEnv(["PACKAGE_NAME=${packageName}"]) {
                            def updatePackage = sh(
                                script: 'grep -q -e "^$PACKAGE_NAME" -e "^lib32-$PACKAGE_NAME" "$REPO_NAME.list"',
                                returnStatus: true
                            )

                            if (updatePackage != 0) {
                                newPackages << it
                            }
                        }
                    }
                }
            }

            pipelineParams.playit_game_id = newPackages
        }
    }

    def artifacts() {
        dir("game") {
            archiveArtifacts artifacts: '*.pkg.tar.*', fingerprint: true, onlyIfSuccessful: true
        }
    }
}
