package pl.szafar.jenkins.linux

import pl.szafar.jenkins.DockerStep

abstract class LinuxDeploy extends DockerStep {
    def common
    def repoPath
    def defaultRepoName = ""
    def defaultRepoPath = ""
    def agentLabel = []
    def pipelineParams = [
        cleanLists: false,
        addonalLabel: [],
    ]

    def LinuxDeploy(jenkins) {
        super(jenkins)
    }

    def init(body) {
        def argPipelineParams = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = argPipelineParams
        body()

        pipelineParams << argPipelineParams

        if (pipelineParams.addonalLabel instanceof String) {
            pipelineParams.addonalLabel = [ pipelineParams.addonalLabel ]
        }

        agentLabel += pipelineParams.addonalLabel

        currentBuild.description = "${params.sourceJob} #${params.sourceJobNumber}"
        if (params.forceAdd) {
            currentBuild.description += " (force add)"
        }

        withFolderProperties {
            dockerImage += env.DOCKER_IMAGE
            dockerArgs = "-v ${params.repoPath}:${repoPath}"
        }
    }

    def deploy_step() {
        runStep {
            prepare()
            artifacts()
            deploy()
            saveRepoList()
        } 
    }

    def cleanup_step() {
        deleteDir()
    }

    def prepare() {
        common.importGpgKey()
    }
    

    def artifacts() {
        copyArtifacts(projectName: params.sourceJob, selector: specific(params.sourceJobNumber))
    }

    def deploy() {}
    def listAllPackagesToFile() {}

    def saveRepoList() {
        if ((currentBuild.previousSuccessfulBuild != null) && !pipelineParams.cleanLists) {
            copyArtifacts(projectName: currentBuild.fullProjectName,
                        selector: specific(currentBuild.previousSuccessfulBuild.number.toString()))
        }

        listAllPackagesToFile()

        archiveArtifacts(artifacts: '*.list', fingerprint: true, onlyIfSuccessful: true)
    }
}
