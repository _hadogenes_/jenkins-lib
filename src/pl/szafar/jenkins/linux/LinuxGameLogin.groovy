package pl.szafar.jenkins.linux

import pl.szafar.jenkins.DockerStep

class LinuxGameLogin extends DockerStep {
    def agentLabel = [ 'x86', 'prebuild' ]

    def pipelineParams = [
        addonalLabel: [],
    ]

    LinuxGameLogin(jenkins) {
        super(jenkins)
    }

    def gog() {
        withFolderProperties {
            copyArtifacts(projectName: env.GAME_LOGIN_JOB)
            def gogLogged = false

            if (fileExists('gog.zip')) {
                sh """
                    mkdir -p '${env.HOME}/.config'
                    unzip -o gog.zip -d '${env.HOME}/.config'
                """

                def gogStatus = sh(
                    script: 'lgogdownloader --list --exclude all < /dev/null > /dev/null 2> /dev/null',
                    returnStatus: true
                )

                gogLogged = (gogStatus == 0)
            }

            if (!gogLogged) {
                runBuildIfNotRunning(true)

                copyArtifacts(projectName: env.GAME_LOGIN_JOB)
                sh """
                    mkdir -p '${env.HOME}/.config'
                    unzip -o gog.zip -d '${env.HOME}/.config'
                """
            }
        }
    }    

    def init(body) {
        def argPipelineParams = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = argPipelineParams
        body()

        pipelineParams << argPipelineParams

        agentLabel += pipelineParams.addonalLabel
    }

    def login_step() {
        runStep {
            if (currentBuild.previousSuccessfulBuild) {
                copyArtifacts(projectName: currentBuild.fullProjectName,
                            selector: specific(currentBuild.previousSuccessfulBuild.number.toString()))
            }

            if (params.gog) {
                gog_login()
            }

            archiveArtifacts(artifacts: '*.zip', fingerprint: true, onlyIfSuccessful: true)
        }
    }

    def getLastBuildWithParam(boolean gog) {
        def job = Jenkins.instance.getItemByFullName(env.GAME_LOGIN_JOB)
        def build = null

        job.getBuilds().reverse().any {
            buildEnv = it.getEnvVars()
            if (gog && (buildEnv.containsKey('gog') && (buildEnv.get('gog') == 'true'))) {
                build = it
                return true // break
            }
        }

        return build
    }

    def runBuildIfNotRunning(boolean gog) {
        // Random wait so corricurent builds won't start build
        sleep(random.nextInt(4096))

        def lastBuild = getLastBuildWithParam(gog)
        if ((lastBuild == null) || (!lastBuild.isBuilding())) {
            build(
                job: env.GAME_LOGIN_JOB,
                parameters: [
                    booleanParam(name: 'gog', value: gog)
                ],
                propagate: true,
                wait: true
            )
            return
        }


        echo("Waiting for build: " + lastBuild.getFullDisplayName() + " - " + lastBuild.getAbsoluteUrl())
        while (lastBuild.isBuilding()) {
            sleep(3000)
        }
    }

    def gog_login() {
//         withFolderProperties {
//             withCredentials([usernamePassword(credentialsId: env.GOG_CREDENTIALS_ID, usernameVariable: 'GOG_USER', passwordVariable: 'GOG_PASSWORD')]) {
//                 sh '''
//                     lgogdownloader \
//                         --login-email "$GOG_USER" --login-password "$GOG_PASSWORD" \
//                         --login > /dev/null 2>&1
//                 '''
//             }
//         }

        notify("Waiting ${currentBuild.projectName}", "Now login to gog on ${env.NODE_NAME}")
        echo("Run on ${env.NODE_NAME}: lgogdownloader --login-email GOG_USER --login-password GOG_PASSWORD --login --enable-login-gui")
        input(id: 'userInput', message: 'Now login')

        sh "mv '${env.HOME}/.config/lgogdownloader' '${env.WORKSPACE}'"
        zip(zipFile: 'gog.zip', overwrite: true, dir: "${env.WORKSPACE}", glob: 'lgogdownloader/*', archive: true)
    }
}
