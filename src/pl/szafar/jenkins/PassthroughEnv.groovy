package pl.szafar.jenkins

abstract class PassthroughEnv {
    def jenkins
    def env
    def currentBuild
    def docker
    def params
    
    PassthroughEnv(def jenkins) {
        this.jenkins = jenkins
        env = jenkins.env
        currentBuild = jenkins.currentBuild
        docker = jenkins.docker
        params = jenkins.params
    }

    def echo(def kwargs) {
        return jenkins.echo(kwargs)
    }

    def error(def kwargs) {
        return jenkins.error(kwargs)
    }

    def dir(String dirname, def kwargs) {
        return jenkins.dir(dirname, kwargs)
    }
    
    def deleteDir() {
        return jenkins.deleteDir()
    }

    def pwd() {
        return jenkins.pwd()
    }
    def pwd(def kwargs) {
        return jenkins.pwd(kwargs)
    }

    def file(def args) {
        return jenkins.file(args)
    }
    
    def string(def args) {
        return jenkins.string(args)
    }
    
    def sh(def kwargs) {
        return jenkins.sh(kwargs)
    }

    def git(def kwargs) {
        return jenkins.git(kwargs)
    }
    
    def input(def kwargs) {
        return jenkins.input(kwargs)
    }

    def specific(def kwargs) {
        return jenkins.specific(kwargs)
    }

    def withFolderProperties(def kwargs) {
        return jenkins.withFolderProperties(kwargs)
    }
    
    def withCredentials(def args, def kwargs) {
        return jenkins.withCredentials(args, kwargs)
    }

    def usernamePassword(def kwargs) {
        return jenkins.usernamePassword(kwargs)
    }

    def sshagent(def args, def kwargs) {
        return jenkins.sshagent(args, kwargs)
    }

    def withEnv(def args, def kwargs) {
        return jenkins.withEnv(args, kwargs)
    }

    def fileExists(def kwargs) {
        return jenkins.fileExists(kwargs)
    }

    def findFiles() {
        return jenkins.findFiles()
    }
    def findFiles(def kwargs) {
        return jenkins.findFiles(kwargs)
    }

    def httpRequest(def kwargs) {
        return jenkins.httpRequest(kwargs)
    }

    def copyArtifacts(def kwargs) {
        return jenkins.copyArtifacts(kwargs)
    }

    def archiveArtifacts(def kwargs) {
        return jenkins.archiveArtifacts(kwargs)
    }

    def wrap(def args, def kwargs) {
        return jenkins.wrap(args, kwargs)
    }

    def booleanParam(def kwargs) {
        return jenkins.booleanParam(kwargs)
    }

    def build(def kwargs) {
        return jenkins.build(kwargs)
    }

    def zip(def kwargs) {
        return jenkins.zip(kwargs)
    }

    def unzip(def kwargs) {
        return jenkins.unzip(kwargs)
    }

    /*
     * URLTrigger
     */
    def URLTriggerEntry(def kwargs) {
        return jenkins.URLTriggerEntry(kwargs)
    }

    def RequestHeader(def kwargs) {
        return jenkins.RequestHeader(kwargs)
    }

    def JsonContent(def kwargs) {
        return jenkins.JsonContent(kwargs)
    }

    def JsonContentEntry(def kwargs) {
        return jenkins.JsonContentEntry(kwargs)
    }

    def XMLContent(def kwargs) {
        return jenkins.XMLContent(kwargs)
    }

    def XMLContentEntry(def kwargs) {
        return jenkins.XMLContentEntry(kwargs)
    }

    def TextContent(def kwargs) {
        return jenkins.TextContent(kwargs)
    }

    def TextContentEntry(def kwargs) {
        return jenkins.TextContentEntry(kwargs)
    }
}
