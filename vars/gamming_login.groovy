import pl.szafar.jenkins.linux.LinuxGameLogin

def call(body) {
    def gameLogin = new LinuxGameLogin(this)
    gameLogin.init(body)

    pipeline {
        agent {
            label gameLogin.agentLabel.join('&&')
        }
        options {
            ansiColor('xterm')
            buildDiscarder(
                logRotator(
                    daysToKeepStr: '7',
                    numToKeepStr: '5',
                )
            )
            disableConcurrentBuilds()
        }
        parameters {
            booleanParam(name: 'gog', description: '', defaultValue: false)
        }
        environment {
            TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
        }
        stages {
            stage('Login') {
                steps {
                    script {
                        gameLogin.login_step()
                    }
                }
                post {
                    cleanup {
                        deleteDir()
                    }
                }
            }
        }
        post {
            fixed {
                script {
                    gameLogin.notifyFixed()
                }
            }
            regression {
                script {
                    gameLogin.notifyRegression()
                }
            }
        }
    }
}
