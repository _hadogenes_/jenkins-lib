import pl.szafar.jenkins.linux.fedora.FedoraBuild
import pl.szafar.jenkins.linux.fedora.FedoraDeploy
// import pl.szafar.jenkins.linux.fedora.FedoraGame

def call(def arg1, def arg2 = null) {
    String type
    def body
    if (arg2 == null) {
        type = "build"
        body = arg1
    }
    else {
        type = arg1
        body = arg2
    }
    if (type == "build") {
        def fedora = new FedoraBuild(this)
        fedora.init(body)

        pipeline {
            agent none
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '7',
                        numToKeepStr: '5',
                        artifactNumToKeepStr: '1'
                    )
                )
                disableConcurrentBuilds()
            }
//            triggers {
//                cron(fedora.triggerCron)
//
//                URLTrigger(
//                    cronTabSpec: fedora.triggerUrlCron,
//                    entries: fedora.urlTriggerEntries
//                )
//            }
            stages {
                stage('PreBuild') {
                    when {
                        beforeAgent true
                        anyOf { 
                            equals expected: false, actual: fedora.pipelineParams.forceBuild
                        }
                    }
                    agent {
                        label fedora.preBuildAgentLabel.join('&&')
                    }
                    environment {
						BUILDDIR = sh(script:'mkdir -p $WORKSPACE/build; echo $WORKSPACE/build', returnStdout: true).trim()
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            fedora.prebuild_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                fedora.cleanup_step()
                            }
                        }
                    }
                }
                stage('Build') {
                    when {
                        beforeAgent true
                        expression { fedora.packagesToBuild.size() > 0 }
                    }
                    agent {
                        label fedora.buildAgentLabel.join('&&')
                    }
                    environment {
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                        BUILDDIR = sh(script:'mkdir -p $WORKSPACE/build; echo $WORKSPACE/build', returnStdout: true).trim()
                        PKGDEST = sh(script:'mkdir -p $HOME/pkgs/$JOB_NAME; echo $HOME/pkgs/$JOB_NAME', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            fedora.build_step()
                        }
                    }
                    post {
                        always {
                            script {
                                fedora.post_step()
                            }
                        }
                        cleanup {
                            script {
                                fedora.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        fedora.notifyFixed()
                    }
                }
                regression {
                    script {
                        fedora.notifyRegression()
                    }
                }
            }
        }
    }
    else if (type == "deploy") {
        def fedora = new FedoraDeploy(this)
        fedora.init(body)

        pipeline {
            agent {
                label fedora.agentLabel.join('&&')
            }
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '14'
                    )
                )
                disableConcurrentBuilds()
            }
            parameters {
                string(name: 'sourceJob', description: '')
                string(name: 'sourceJobNumber', description: '')
                string(name: 'repoName', description: '')
                string(name: 'fedoraRelease', description: '')
                booleanParam(name: 'forceAdd', description: '', defaultValue: false)
            }
            environment {
                TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
            }
            stages {
                stage('Deploy') {
                    steps {
                        script {
                            fedora.deploy_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                fedora.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        fedora.notifyFixed()
                    }
                }
                regression {
                    script {
                        fedora.notifyRegression()
                    }
                }
            }
        }
    }
//     else if (type == "game") {
//         def fedora = new FedoraGame(this)
//         fedora.init(body)
// 
//         pipeline {
//             agent none
//             options {
//                 ansiColor('xterm')
//                 buildDiscarder(
//                     logRotator(
//                         daysToKeepStr: '7',
//                         numToKeepStr: '5',
//                         artifactNumToKeepStr: '1'
//                     )
//                 )
//                 disableConcurrentBuilds()
//             }
//             triggers {
//                 cron('0 12 * * 6')
//             }
//             stages {
//                 stage('PreBuild') {
//                     agent {
//                         label fedora.preBuildAgentLabel.join('&&')
//                     }
//                     environment {
//                         TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
//                     }
//                     steps {
//                         script {
//                             fedora.pregame_step()
//                         }
//                     }
//                     post {
//                         cleanup {
//                             script {
//                                 fedora.cleanup_step()
//                             }
//                         }
//                     }
//                 }
//                 stage('Game') {
//                     when {
//                         beforeAgent true
//                         allOf { 
//                             equals expected: true, actual: fedora.playit
//                             expression { fedora.playit_game_id.size() > 0 }
//                         }       
//                     }
//                     agent {
//                         label buildAgentLabel.join('&&')
//                     }
//                     environment {
//                         TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
//                     }
//                     steps {
//                         script {
//                             fedora.game_step()
// 
//                             withFolderProperties {
//                                 build(
//                                     job: env.DEPLOY_JOB,
//                                     parameters: [
//                                         string(name: 'sourceJob', value: env.JOB_NAME),
//                                         string(name: 'sourceJobNumber', value: env.BUILD_NUMBER),
//                                         string(name: 'repoName', value: env.REPO_NAME),
//                                         string(name: 'repoPath', value: env.REPO_PATH),
//                                         booleanParam(name: 'forceAdd', value: fedora.pipelineParams.forceBuild)
//                                     ],
//                                     propagate: false,
//                                     wait: false
//                                 )
//                             }
//                         }
//                     }
//                     post {
//                         cleanup {
//                             script {
//                                 fedora.cleanup_step()
//                             }
//                         }
//                     }
//                 }
//             }
//             post {
//                 fixed {
//                     script {
//                         fedora.notifyFixed()
//                     }
//                 }
//                 regression {
//                     script {
//                         fedora.notifyRegression()
//                     }
//                 }
//             }
//         }
//     }
    else {
        error("Wrong type: " + type)
    }
}
