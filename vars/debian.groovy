import pl.szafar.jenkins.linux.debian.DebianBuild
import pl.szafar.jenkins.linux.debian.DebianDeploy
import pl.szafar.jenkins.linux.debian.DebianGame

def call(def arg1, def arg2 = null) {
    String type
    def body
    if (arg2 == null) {
        type = "build"
        body = arg1
    }
    else {
        type = arg1
        body = arg2
    }
    if (type == "build") {
        def debian = new DebianBuild(this)
        debian.init(body)

        pipeline {
            agent none
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '7',
                        numToKeepStr: '5',
                        artifactNumToKeepStr: '1'
                    )
                )
                disableConcurrentBuilds()
            }
            triggers {
                cron(debian.triggerCron)

                URLTrigger(
                    cronTabSpec: debian.triggerUrlCron,
                    entries: debian.urlTriggerEntries
                )
            }
            stages {
                stage('PreBuild') {
                    when {
                        beforeAgent true
                        anyOf { 
                            equals expected: false, actual: debian.pipelineParams.forceBuild
                        }
                    }
                    agent {
                        label debian.preBuildAgentLabel.join('&&')
                    }
                    environment {
                        BUILDDIR = sh(script:'mkdir -p $WORKSPACE/build; echo $WORKSPACE/build', returnStdout: true).trim()
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            debian.prebuild_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                debian.cleanup_step()
                            }
                        }
                    }
                }
                stage('Build') {
                    when {
                        beforeAgent true
                        expression { debian.packagesToBuild.size() > 0 }
                    }
                    agent {
                        label debian.buildAgentLabel.join('&&')
                    }
                    environment {
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                        BUILDDIR = sh(script:'mkdir -p $WORKSPACE/build; echo $WORKSPACE/build', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            debian.build_step()
                        }
                    }
                    post {
                        always {
                            script {
                                debian.post_step()
                            }
                        }
                        cleanup {
                            script {
                                debian.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        debian.notifyFixed()
                    }
                }
                regression {
                    script {
                        debian.notifyRegression()
                    }
                }
            }
        }
    }
    else if (type == "deploy") {
        def debian = new DebianDeploy(this)
        debian.init(body)

        pipeline {
            agent {
                label debian.agentLabel.join('&&')
            }
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '14'
                    )
                )
                disableConcurrentBuilds()
            }
            parameters {
                string(name: 'sourceJob', description: '')
                string(name: 'sourceJobNumber', description: '')
                string(name: 'repoName', description: '')
                string(name: 'debianRelease', description: '')
                booleanParam(name: 'forceAdd', description: '', defaultValue: false)
            }
            environment {
                TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
            }
            stages {
                stage('Deploy') {
                    steps {
                        script {
                            debian.deploy_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                debian.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        debian.notifyFixed()
                    }
                }
                regression {
                    script {
                        debian.notifyRegression()
                    }
                }
            }
        }
    }
    else if (type == "game") {
        def debian = new DebianGame(this)
        debian.init(body)

        pipeline {
            agent none
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '7',
                        numToKeepStr: '5',
                        artifactNumToKeepStr: '1'
                    )
                )
                disableConcurrentBuilds()
            }
            triggers {
                cron('0 12 * * 6')
            }
            stages {
                stage('PreBuild') {
                    when {
                        beforeAgent true
                        anyOf {
                            equals expected: false, actual: debian.pipelineParams.forceBuild
                        }
                    }
                    agent {
                        label debian.preBuildAgentLabel.join('&&')
                    }
                    environment {
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            debian.pregame_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                debian.cleanup_step()
                            }
                        }
                    }
                }
                stage('Game') {
                    when {
                        beforeAgent true
                        allOf {
                            equals expected: true, actual: debian.pipelineParams.playit
                            expression { debian.pipelineParams.playit_game_id.size() > 0 }
                        }
                    }
                    agent {
                        label  debian.buildAgentLabel.join('&&')
                    }
                    environment {
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            debian.game_step()
                        }
                    }
                    post {
                        always {
                            script {
                                debian.post_step()
                            }
                        }
                        cleanup {
                            script {
                                debian.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        debian.notifyFixed()
                    }
                }
                regression {
                    script {
                        debian.notifyRegression()
                    }
                }
            }
        }
    }
    else {
        error("Wrong type: " + type)
    }
}
