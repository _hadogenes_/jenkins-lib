import pl.szafar.jenkins.DockerBuild

def call(body) {
    def dockering = new DockerBuild(this)
    dockering.init(body)

    pipeline {
        agent {
            label dockering.agentLabel.join('&&')
        }
        options {
            ansiColor('xterm')
            buildDiscarder(
                logRotator(
                    daysToKeepStr: '7',
                    numToKeepStr: '5'
                )
            )
            disableConcurrentBuilds()
        }
        triggers {
            cron('H 1 * * *')
        }
        stages {
            stage('Build') {
                steps {
                    script {
                        dockering.build_step()
                    }
                }
                post {
                    cleanup {
                        script {
                            dockering.cleanup_step()
                        }
                    }
                }
            }
        }
        post {
            fixed {
                script {
                    dockering.notifyFixed()
                }
            }
            regression {
                script {
                    dockering.notifyRegression()
                }
            }
        }
    }
}
