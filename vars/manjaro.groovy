import pl.szafar.jenkins.linux.manjaro.ManjaroBuild
import pl.szafar.jenkins.linux.manjaro.ManjaroDeploy
import pl.szafar.jenkins.linux.manjaro.ManjaroGame

def call(def arg1, def arg2 = null) {
    String type
    def body
    if (arg2 == null) {
        type = "build"
        body = arg1
    }
    else {
        type = arg1
        body = arg2
    }

    if (type == "deploy") {
        def manjaro = new ManjaroDeploy(this)
        manjaro.init(body)

        pipeline {
            agent {
                label manjaro.agentLabel.join('&&')
            }
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '14'
                    )
                )
                disableConcurrentBuilds()
            }
            parameters {
                string(name: 'sourceJob', description: '')
                string(name: 'sourceJobNumber', description: '')
                string(name: 'repoName', description: '', defaultValue: manjaro.defaultRepoName)
                string(name: 'repoPath', description: '', defaultValue: manjaro.defaultRepoPath)
                booleanParam(name: 'forceAdd', description: '', defaultValue: false)
            }
            environment {
                HOME = sh(script:'echo $WORKSPACE', returnStdout: true).trim()
                TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
            }
            stages {
                stage('Deploy') {
                    steps {
                        script {
                            manjaro.deploy_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                manjaro.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        manjaro.notifyFixed()
                    }
                }
                regression {
                    script {
                        manjaro.notifyRegression()
                    }
                }
            }
        }
    }
    else if (type == "game") {
        def manjaro = new ManjaroGame(this)
        manjaro.init(body)

        pipeline {
            agent none
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '7',
                        numToKeepStr: '5',
                        artifactNumToKeepStr: '1'
                    )
                )
                disableConcurrentBuilds()
            }
//             triggers {
//                 cron('0 12 * * 6')
//             }
            stages {
                stage('PreBuild') {
                    agent {
                        label manjaro.preBuildAgentLabel.join('&&')
                    }
                    environment {
                        HOME = sh(script:'echo $WORKSPACE', returnStdout: true).trim()
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            manjaro.pregame_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                manjaro.cleanup_step()
                            }
                        }
                    }
                }
                stage('Game') {
                    when {
                        beforeAgent true
                        allOf { 
                            equals expected: true, actual: manjaro.playit
                            expression { manjaro.playit_game_id.size() > 0 }
                        }       
                    }
                    agent {
                        label buildAgentLabel.join('&&')
                    }
                    environment {
                        HOME = sh(script:'echo $WORKSPACE', returnStdout: true).trim()
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            manjaro.game_step()

                            withFolderProperties {
                                build(
                                    job: env.DEPLOY_JOB,
                                    parameters: [
                                        string(name: 'sourceJob', value: env.JOB_NAME),
                                        string(name: 'sourceJobNumber', value: env.BUILD_NUMBER),
                                        string(name: 'repoName', value: env.REPO_NAME),
                                        string(name: 'repoPath', value: env.REPO_PATH),
                                        booleanParam(name: 'forceAdd', value: manjaro.pipelineParams.forceBuild)
                                    ],
                                    propagate: false,
                                    wait: false
                                )
                            }
                        }
                    }
                    post {
                        cleanup {
                            script {
                                manjaro.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        manjaro.notifyFixed()
                    }
                }
                regression {
                    script {
                        manjaro.notifyRegression()
                    }
                }
            }
        }
    }
    else if (type == "build") {
        def manjaro = new ManjaroBuild(this)
        manjaro.init(body)

        pipeline {
            agent none
            options {
                ansiColor('xterm')
                buildDiscarder(
                    logRotator(
                        daysToKeepStr: '7',
                        numToKeepStr: '5',
                        artifactNumToKeepStr: '1'
                    )
                )
                disableConcurrentBuilds()
            }
//             triggers {
//                 cron(manjaro.triggerCron)
// 
//                 URLTrigger(
//                     cronTabSpec: manjaro.triggerUrlCron,
//                     entries: manjaro.urlTriggerEntries
//                 )
//             }
            stages {
                stage('PreBuild') {
                    when {
                        beforeAgent true
                        anyOf { 
                            equals expected: false, actual: manjaro.pipelineParams.forceBuild
                            equals expected: true, actual: manjaro.pipelineParams.aur
                        }
                    }
                    agent {
                        label manjaro.preBuildAgentLabel.join('&&')
                    }
                    environment {
                        HOME = sh(script:'echo $WORKSPACE', returnStdout: true).trim()
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            manjaro.prebuild_step()
                        }
                    }
                    post {
                        cleanup {
                            script {
                                manjaro.cleanup_step()
                            }
                        }
                    }
                }
                stage('Build') {
                    when {
                        beforeAgent true
                        expression { manjaro.packagesToBuild.size() > 0 }
                    }
                    agent {
                        label manjaro.buildAgentLabel.join('&&')
                    }
                    environment {
                        HOME = sh(script:'echo $WORKSPACE', returnStdout: true).trim()
                        TMPDIR = sh(script:'mkdir -p $WORKSPACE/tmp; echo $WORKSPACE/tmp', returnStdout: true).trim()
                        PKGDEST = sh(script:'mkdir -p $WORKSPACE/pkgs; echo $WORKSPACE/pkgs', returnStdout: true).trim()
                    }
                    steps {
                        script {
                            manjaro.build_step()
                        }

                        withFolderProperties {
                            build(
                                job: env.DEPLOY_JOB,
                                parameters: [
                                    string(name: 'sourceJob', value: env.JOB_NAME),
                                    string(name: 'sourceJobNumber', value: env.BUILD_NUMBER),
                                    string(name: 'repoName', value: env.REPO_NAME),
                                    string(name: 'repoPath', value: env.REPO_PATH),
                                    booleanParam(name: 'forceAdd', value: manjaro.pipelineParams.forceBuild)
                                ],
                                propagate: false,
                                wait: false
                            )
                        }
                    }
                    post {
                        cleanup {
                            script {
                                manjaro.cleanup_step()
                            }
                        }
                    }
                }
            }
            post {
                fixed {
                    script {
                        manjaro.notifyFixed()
                    }
                }
                regression {
                    script {
                        manjaro.notifyRegression()
                    }
                }
            }
        }
    }
    else {
        error("Wrong type: " + type)
    }
}
